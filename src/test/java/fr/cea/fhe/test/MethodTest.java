package fr.cea.fhe.test;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.cea.fhe.services.BlackListAnalysisServices;
import static org.junit.Assert.assertEquals;

import java.util.List;

@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT)
@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("test") // load application-test.properties
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class MethodTest {
	@Test
    public void testExtractFieldIP() {

        System.out.println("Exec test extract encrypted ip from CEF file ");

//		String ctifFile = 
//        		"CEF:0|Router_Vendor|Router_CED|1.0|100|Connection Detected|5|src=192.168.1.2 "
//        		+ "spt=24920 dst=1:83.220.94.73 dpt=22126 proto=UDP end=1505462160000 "
//        		+ "dtz=Europe/Berlin\nCEF:0|Router_Vendor|Router_CED|1.0|100|Connection Detected|5|src=192.168.1.3 spt=22126 "
//        		+ "dst=1:54.213.116.149 dpt=24920 proto=TCP end=1505462161000 dtz=Europe/Berlin";
//        String expectedOutput = "83.220.94.73|54.213.116.149|";
//        
//        BlackListServices bls = new BlackListServices();
//        List<String> rest = bls.__getIpFromEncryptedPattern("dst", ctifFile);
//        assertEquals(2, rest.size());
//        String resIp = "";
//        for(String ip : rest)
//        {
//        	resIp += ip +"|";
//        		
//        }
//        System.out.println("IP SIZE = "+ rest.size() +" : to retrieve " + resIp + " <> " + expectedOutput);
//        assertEquals(expectedOutput, resIp);
    }
}
