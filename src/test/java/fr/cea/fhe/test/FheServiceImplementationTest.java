/**
 *  Copyright 2017 Hewlett Packard Enterprise Development Company, L.P.
 */
package fr.cea.fhe.test;

import org.apache.commons.io.FileUtils;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import fr.cea.fhe.helper.BashTools;
import fr.cea.fhe.helper.Constants;
import fr.cea.fhe.helper.ToolControlers;
import fr.cea.fhe.models.DpoResponses;
import fr.cea.fhe.restapi.FheServiceImplementation;
import fr.cea.fhe.services.BlackListAnalysisServices;
import fr.cea.fhe.services.FheAnalysisServices;
import fr.cea.fhe.type.RestResponse;
import fr.cea.fhe.type.xacml.RequestAttributes;
import fr.cea.fhe.type.xacml.RequestContainer;
import fr.cea.fhe.type.xacml.RequestElement;

// for code completion add MockMvcRequestBuilders and MockMvcRequestBuilders as 'favorite types' in eclipse
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*; //get, post
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Base64;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * @author MIMANE
 *
 */
@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT)
@RunWith(SpringRunner.class)
@ActiveProfiles("test") // load application-test.properties
@AutoConfigureMockMvc
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class FheServiceImplementationTest {

	protected Logger log = LoggerFactory.getLogger(FheServiceImplementationTest.class.getName());	

	
    @Autowired
    private MockMvc mockMvc;
    
    @Value("${security.user.name}")
    private String restUser;
    @Value("${security.user.password}")
    private String restPassword;
    
    @Value("${iai.fhe.dedicatedDsaId}")
	String DSA_ID; //"DSA-44ee3dfc-4d3a-4219-979a-eb6ba7a8efc2";
			//"DSA-e3d5bc24-18dd-41f1-ba6c-5f7992fb71d0";
			//"DSA-1f6fe910-f595-4c34-8b9c-837202db5f3a";
    
    String testReadMetadata = "{   \"Request\": {     \"Attribute\": [       {         \"AttributeId\": \"urn:oasis:names:tc:xacml:1.0:subject:subject-id\",         "
    		+ "\"Value\": \"user2\",         \"DataType\": \"string\"       },       "
    		+ "{         \"AttributeId\": \"urn:oasis:names:tc:xacml:1.0:subject:authentication-type\",         \"Value\": \"low\",         "
    		+ "\"DataType\": \"string\"       },       {         \"AttributeId\": \"urn:oasis:names:tc:xacml:1.0:action:action-id\",        "
    		+ " \"Value\": \"read\",         \"DataType\": \"string\"       },       {         "
    		+ "\"AttributeId\": \"urn:oasis:names:tc:xacml:3.0:subject:access-purpose\",         \"Value\": \"Cyber Threat Monitoring\",         "
    		+ "\"DataType\": \"string\"       },       {         \"AttributeId\": \"ns:c3isp:aggregation-operation\",         \"Value\": \"empty\",         "
    		+ "\"DataType\": \"string\"       }     ]   } }";
    	
	private String testMetadata = "{            "
			+ "\"id\" : \"4000123\",       "
			+ "\"dsa_id\" :  \"{DSA_ID}\",       "
			+ "\"start_time\" : \"2017-12-14T12:00:00.0Z\",      "
			+ "\"end_time\" : \"2017-12-14T18:01:01.0Z\",       "
			+ "\"event_type\" : \"Firewall Event\",       "
			+ "\"fhe_data\":\"true\",       "
			+ "\"organization\" : \"ISP@CNR\" }";
	
	File fileZip = new File("/home/nguyen/81677462-d679-44a0-aed6-2a48d993a3f29166557881037228422.zip");
	
	
	String isiApiURL = "https://isic3isp.iit.cnr.it:8443/isi-api";
	String publishApi = "/v1/dpo";
	
    //@Test	
	public void testPrepareCreateDPO() throws IOException {
    	log.info("Test Create DPO from remote connection ");
    	if(fileZip.exists() == false)
    	{
    		log.info("File local test is not existed ! This test is dedicated to local test, not for production");
    		return;
    	}
    	
    	String	_tmpMetadata = testMetadata.replaceAll("\\{" + "DSA_ID" + "\\}", this.DSA_ID);
    	String dposId = ToolControlers.createDpoObject(isiApiURL+publishApi+"?norm=false", fileZip, _tmpMetadata,  this.DSA_ID);
    	
    	System.err.println("dpo_id is: " + dposId);    	
    }
    
    @Test	
	public void testReadDPO() throws IOException {
    	log.info("Test Create DPO from remote connection ");
    	if(fileZip.exists() == false)
    	{
    		log.info("File local test is not existed ! This test is dedicated to local test, not for production");
    		return;
    	}
    	
    	String[] arrDposId = new String [] {
    			//"1570094989166-6dc49f6f-1319-4226-9e90-b758564a2f71",
			"1573726173319-f0443741-f9c8-4773-b417-48c05cab72c2",
			"1573726171491-9a42545b-e3d8-4387-af17-eaacc3551cfe",
			"1573726188566-c71f86da-46e2-462c-986f-936a160ee6b4",
		      "1573726186382-8c934634-d067-4a1b-8aff-cbdc57b6faec",    		    
		      "1573726196727-e554d82c-7ceb-44a3-b67d-a0d84554c34f",
		      "1573726195111-e4894939-bc1c-4607-b465-b50034f5067c",
		      "1573726206692-51bb9d5e-9684-49b0-ba4f-5f93535067f9",
		      "1573726205068-7101777a-8f0f-43d2-88eb-0714f60cc43f"
    	};
		
///////////////////////////////////////
// READ
///////////////////////////////////////
				
		
		//URL fileURL = this.getClass().getClassLoader().getResource(INPUT_FILE);
		String	_tmpMetadata = testMetadata.replaceAll("\\{" + "DSA_ID" + "\\}", this.DSA_ID);
		ObjectMapper mapper = new ObjectMapper();
		Map<String, Object> jsonMap = mapper.readValue(_tmpMetadata,
		    new TypeReference<Map<String,Object>>(){});
		
		String plainCreds = "user:password";
		byte[] plainCredsBytes = plainCreds.getBytes();
		byte[] base64CredsBytes = Base64.getEncoder().encode(plainCredsBytes);
		String base64Creds = new String(base64CredsBytes);

		MultiValueMap<String, Object> parts1 = new LinkedMultiValueMap<String, Object>();

//		parts.add("input_metadata", container);
//		parts.add("fileToSubmit", new FileSystemResource(new File(fileURL.getFile())) );
		
	
		
		HttpHeaders headers1 = new HttpHeaders();
		headers1.add("Authorization", "Basic " + base64Creds);
//		headers1.add("X-c3isp-input_metadata", URLEncoder.encode("{}", "UTF-8"));
		//headers1.add("X-c3isp-input_metadata", testReadMetadata);
		headers1.add("X-c3isp-input_metadata", "{}");
			
//		headers.setContentType(MediaType.MULTIPART_FORM_DATA);
		HttpEntity<MultiValueMap<String, Object>> request1 = 
				new HttpEntity<MultiValueMap<String, Object>>(parts1, headers1);
		
		System.err.println(request1);
		
//		ResponseEntity<FileSystemResource> readResponse = restTemplate//.withBasicAuth("user", "password")
//				.exchange(
//						isiApiURL+publishApi+"/"+dposId+"/", 
//						HttpMethod.GET, 
//						request1, 
//						FileSystemResource.class);
		
		System.err.println("READ metadata");
		RestTemplate restTemplate = ToolControlers.createSSLIgnoringRestTemplate();
		
		for (String dposId : arrDposId) {		
			ResponseEntity<ByteArrayResource> readResponse = restTemplate//.withBasicAuth("user", "password")
			.exchange(
					isiApiURL+publishApi+"/"+dposId+"/", 
					HttpMethod.GET, 
					request1, 
					ByteArrayResource.class);
			
			
			ByteArrayResource file = readResponse.getBody(); 
			System.err.println("Content = " + new String(file.getByteArray(), StandardCharsets.UTF_8));
			File tmpFile = File.createTempFile("udd", ".fff");
			FileUtils.forceDeleteOnExit(tmpFile);
			FileUtils.copyInputStreamToFile(file.getInputStream(), tmpFile);
			
			System.err.println("file was stored at " + tmpFile.getAbsolutePath());		
		}
		//assertTrue(FileUtils.contentEquals(tmpFile, new File(fileURL.getFile())));
				
    }
        
	
	

	//@Test
    public void test01getDstIp() throws Exception {
		log.info("Test Create DPO from remote connection ");
    	String m_metadata = "{\n" + 
    			"  \"Request\": {\n" + 
    			"    \"Attribute\": [\n" + 
    			"      {\n" + 
    			"        \"AttributeId\": \"ns:c3isp:dsa-id\",\n" + 
    			"        \"Value\": \""+ DSA_ID +"\",\n" + 
    			"        \"DataType\": \"string\"\n" + 
    			"      },\n" + 
    			"      {\n" + 
    			"        \"AttributeId\": \"ns:c3isp:dpo-metadata\",\n"
    			+ "    \"Value\":\"{\\\"id\\\":\\\"9924000123_CEA\\\","
    			+ "                 \\\"dsa_id\\\":\\\""+ DSA_ID +"\\\","
    			+ "                 \\\"start_time\\\":\\\"2019-10-02T16:00:00.0Z\\\","
    			+ "                 \\\"end_time\\\":\\\"2019-10-02T17:01:01.0Z\\\","
    			+ "                 \\\"event_type\\\":\\\"Firewall Event\\\",\\\"organization\\\":\\\"ISP@CNR\\\"}\"," 
    			+ "        \"DataType\": \"string\"\n" + 
    			"      }\n" + 
    			"    ]\n" + 
    			"  }\n" + 
    			"}";

    	
    	File fileZip = new File("/home/nguyen/81677462-d679-44a0-aed6-2a48d993a3f29166557881037228422.zip");
    	log.info("Invoke ISI create DPO for this result !");
		String dpoId = ToolControlers.createDpoObject(isiApiURL+publishApi, fileZip, m_metadata, DSA_ID);
		log.info("dpoId ISI create DPO = " + dpoId);
		assertTrue(dpoId.length() > 0);
    	
//        this.mockMvc.perform(
//                get("/v1/template/" + param + "/")
//                    .with(httpBasic(restUser, restPassword)) // basic auth
//                    .accept(MediaType.APPLICATION_JSON)
//                    .contentType(MediaType.APPLICATION_JSON)
//                 )
//                .andDo(MockMvcResultHandlers.print())
//                .andExpect(status().isOk())
//                .andExpect(content().string(containsString(expectedOutput))) //check if the returned output (json) contains 'expectedOutput
//                ;
    }
	
	@Test
    public void testInvokeFHEBlacklistAnalysis() throws Exception {
		log.info("Execution TESTTTTTTTTTTTTTTTT");
		log.info(">>>>>>>>>>> testInvokeFHEBlacklistAnalysis <<<<<<<<<<<<<<<<<<<");		
		String prefixVDL = "file://";
        String paramVDL = "/opt/isi/datalakebuffer/c3316973-0a76-4bd6-95db-0b0483868cb8";
		//String paramVDL = "file:///opt/isi/datalakebuffer/09951ad6-24de-4a73-b29e-2002964c2b46";
		String method = "BLACK_LIST_LOW";
        //String paramVDL = "file:///opt/isi/datalakebuffer/a74d0ae9-adce-4412-8e8f-c036bc1e7b8d"; 
        BashTools.Exec(false, "/bin/rm", "-fr", paramVDL + File.separator + "*"+Constants.FHE_ZIP_EXTENSION);        
        
        File vdlFolder = new File(paramVDL.replace(prefixVDL, ""));  
        if(vdlFolder.exists() == false)
        {
        	log.info("Folder is not existed ! " + paramVDL);
        	return;
        }
		if(vdlFolder.isDirectory())
		{
			File[] dpoFiles = vdlFolder.listFiles();
			for (File file : dpoFiles) {
				if(file.isDirectory())
				{
					log.info("Please remove this folder " + file.getAbsolutePath());
				}
			}			
		}
                       
        String request = String.format("/v1/analyseWithVDL/%1$s?vdlUrl=%2$s", method, paramVDL);
        log.info("Request = " + request);
        //MvcResult result = 
        this.mockMvc.perform(
              post(request)
                  .with(httpBasic(restUser, restPassword)) // basic auth
                  .accept(MediaType.APPLICATION_JSON)
                  .contentType(MediaType.APPLICATION_JSON)
               )
              .andDo(MockMvcResultHandlers.print())
              .andExpect(status().isAccepted());
              //.andReturn();
        
//        String content = result.getResponse().getContentAsString();
//        log.info("Response : " + content);
//        
        //expected output structure = "{\"name\":\"aName\",\"id\":\"anIdValue\",\"path\":\"aPath\",\"version\":\"1\"}";
        
//        this.mockMvc.perform(
//                get("/v1/template/" + param + "/")
//                    .with(httpBasic(restUser, restPassword)) // basic auth
//                    .header("X-myheader-test1", "TEST1")
//                    .header("X-myheader-test2", "TEST2")
//                    .accept(MediaType.APPLICATION_JSON)
//                    .contentType(MediaType.APPLICATION_JSON)
//                 )
//                .andDo(MockMvcResultHandlers.print())
//                .andExpect(status().isOk())
//                .andExpect(jsonPath("$.name", is("aName")))
//                ;
        
    }
}
