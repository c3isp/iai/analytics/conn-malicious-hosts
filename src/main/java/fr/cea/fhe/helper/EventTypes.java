package fr.cea.fhe.helper;

/**
 * @author Francesco Di Cerbo <francesco.di.cerbo@sap.com> 
 *
 */
public interface EventTypes {
	
	
	/**
	 * tryaccess
	 *   additionalProperties: 
     	 * 	 xacmlPolicy: xacml policy
	 *       xacmlRequest: xacml request
	 *       messageId: message-id to identify a try access request/response
	 */
	public final static String TRY_ACCESS = "t";
	
	/**
	 * try access response
         * additionalProperties
	 *  message-id : equal to try access message, 
         *      identifies a response to a previous request
	 *  response: true-false
	 * sessionId: new sessionId to be used from now on
	 * 
	 */
	public final static String TRY_ACCESS_RESPONSE = "ns";
	
	/**
	 * sessionId
	 */
	public final static String START_ACCESS = "s";
	/**
	 * sessionId
	 */
	public final static String END_ACCESS = "e";
	/**
	 * sessionId
	 */
	public final static String ON_REVOKE = "r";
	
	/**
	 * sessionId
	 */
	public final static String RETRIEVE_CONTEXT = "rc";
	/**
	 * xacml request enriched with all available attributes
     	 * additionalProperties:
     	 *  [for each attribute:]
     	 *  attributeId : attributeValue
     	 *  [end for]
	 */
	public final static String RETRIEVE_CONTEXT_RESPONSE = "rcr";
	
	/**
	 * FHE process
	 *   additionalProperties: 
     * 	 	metadataFile: string with metadata (CONTENT in BINARY format :  base64-encoded characters)
	 *      ctiFile: binary content (CONTENT in BINARY format :  base64-encoded characters)
	 *      dsaId: message-id to identify a try access request/response
	 */
	public final static String FHE_ANALYSIS_PROCESS = "fap";

	/**
	 * Bundle Manager Create Response
	 *   additionalProperties: 
     * 	 	dposId: string 
	 */
	public final static String FHE_ANALYSIS_PROCESS_RESPONSE = "fapr";
		
}

