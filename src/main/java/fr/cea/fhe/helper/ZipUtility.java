package fr.cea.fhe.helper;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;
 
/**
 * This utility extracts files and directories of a standard zip file to
 * a destination directory.
 * @author www.codejava.net
 *
 */

public class ZipUtility {
	/**
     * Size of the buffer to read/write data
     */
    //private static final int BUFFER_SIZE = 4096;
	private static final int BUFFER_SIZE = 128;
    /**
     * Extracts a zip file specified by the zipFilePath to a directory specified by
     * destDirectory (will be created if does not exists)
     * @param zipFilePath
     * @param destDirectory
     * @throws IOException
     */
    public static void unzip(String zipFilePath, String destDirectory) throws IOException {
        File destDir = new File(destDirectory);
        if (!destDir.exists()) {
            destDir.mkdir();
        }
        ZipInputStream zipIn = new ZipInputStream(new FileInputStream(zipFilePath));
        ZipEntry entry = zipIn.getNextEntry();
        // iterates over entries in the zip file
        while (entry != null) {
        	if(entry.getName().length() == 0) {
        		System.out.println("ERROR Extracting file has noname !");
        		entry = zipIn.getNextEntry();
        		continue;
        	}
            String filePath = destDirectory + File.separator + entry.getName();
            System.out.println("Starting Extract file " + filePath);
            if (!entry.isDirectory()) {
                // if the entry is a file, extracts it
                extractFile(zipIn, filePath);
            } else {
                // if the entry is a directory, make the directory
                File dir = new File(filePath);
                dir.mkdir();
            }
            System.out.println("End Extract file " + filePath);
            zipIn.closeEntry();
            entry = zipIn.getNextEntry();
        }
        zipIn.close();
    }
    /**
     * Extracts a zip entry (file entry)
     * @param zipIn
     * @param filePath
     * @throws IOException
     */
    private static void extractFile(ZipInputStream zipIn, String filePath) throws IOException {
        BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(filePath));
        byte[] bytesIn = new byte[BUFFER_SIZE];
        //byte[] bytesIn = new byte[4096];        
        int read = 0;
        while ((read = zipIn.read(bytesIn)) != -1) {
            bos.write(bytesIn, 0, read);
        }
        bos.close();
    }    	
	
	public static File zipContent(List<File> srcFiles, String fileName) throws FileNotFoundException, IOException {
		File file = __zipFile(srcFiles, fileName);		
		return file;
	}
	
	private static File __zipFile(List<File> srcFiles, String fileName) throws FileNotFoundException, IOException {
		Path path = Files.createTempFile(fileName, ".zip");
        File file = path.toFile();
        
		FileOutputStream   fos = new FileOutputStream(file);
		ZipOutputStream zos = new ZipOutputStream(fos);
		byte[] buffer = new byte[128];
		for (File currentFile : srcFiles) {
			if (!currentFile.isDirectory()) {
				ZipEntry entry = new ZipEntry(currentFile.getName());
				FileInputStream fis = new FileInputStream(currentFile);
				zos.putNextEntry(entry);
				int read = 0;
				while ((read = fis.read(buffer)) != -1) {
					zos.write(buffer, 0, read);
				}
				zos.closeEntry();
				fis.close();
			}
		}
		zos.close();
		fos.close();
		return file;
	}
	
	/* Create a directory. */
	private static void createDir(String dirPath)
	{
		if(dirPath!=null && !"".equals(dirPath.trim()))
		{
			File dirFile = new File(dirPath);
			
			if(!dirFile.exists())
			{
				boolean result = dirFile.mkdir();
				if(!result)
				{
					System.err.println("Create " + dirPath + " fail. ");
				}
			}
//			else
//			{
//				System.out.println(dirPath + " exist. ");
//			}
		}
	}
}
