package fr.cea.fhe.helper;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ToolsFolder {
	private static final Logger log = LoggerFactory.getLogger(ToolsFolder.class);

	private static String m_threadName = "[FHE - ToolsFolder]: ";
	
	public static File createTempFile(String fileName, String suffix)
			throws IOException, FileNotFoundException {
		// Since Java 1.7 Files and Path API simplify operations on files
		Path path = Files.createTempFile(fileName, suffix);
        File file = path.toFile();
        // This tells JVM to delete the file on JVM exit.
        // Useful for temporary files in tests.
        file.deleteOnExit();
        return file;
	}    
	
	public static File createTempFile(String fileName, String suffix, String strObjectContent)
			throws IOException, FileNotFoundException {
		return createTempFile(fileName, suffix, strObjectContent.getBytes());
	}
	
	public static File createTempFile(String fileName, String suffix, 
			byte[] fileObjectContent) throws IOException {
		Path path = Files.createTempFile(fileName, suffix);
        File file = path.toFile();
        // writing sample data
        Files.write(path, fileObjectContent);
        // This tells JVM to delete the file on JVM exit.
        // Useful for temporary files in tests.
        file.deleteOnExit();
        return file;		
	}
	
	public static String CreateAllFolderNames(String storagePath) throws SecurityException, IOException  {
		//System.out.println("Process to create folder for < " + storagePath + " >");
		File file = new File(storagePath);
		if (!file.exists()) {
			if(file.mkdirs())
			{
				log.info(m_threadName + "Create user directory for FHE encryption purpose : " + storagePath);
			}
			else	
			{					
				System.out.println("Impossible to create user directory " + storagePath + " at " + file.getAbsolutePath());
				return null;
			}			
		}
		return file.getAbsolutePath();
	}	
}
