
package fr.cea.fhe.helper;

import java.io.IOException;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.databind.ObjectMapper;


//@XmlRootElement(name = "Event")
public class Event {

	/**
     * Every request is identified by a requestID
     * (Required)
     * 
     */
//	private String requestID;
//	
//    public String getRequestID() {
//		return requestID;
//	}
//
//	public void setRequestID(String requestID) {
//		this.requestID = requestID;
//	}

	public Event() {
		super();
		additionalProperties = new HashMap<String, String>();
	}

	/**
     * 
     * (Required)
     * necessary for continuous authorization
     * 
     */
//	@XmlElement(name = "sessionID")
    private String sessionID;
    /**
     * 
     * (Required)
     * 
     */
//	@XmlElement(name = "eventType")
    private String eventType;

//	@XmlElementWrapper(name = "additionalProperties")
//	@XmlElement(name="property")
    private Map<String, String> additionalProperties;

    /**
     * 
     * (Required)
     * 
     * @return
     *     The sessionID
     */
    public String getSessionID() {
        return sessionID;
    }

    /**
     * 
     * (Required)
     * 
     * @param sessionID
     *     The sessionID
     */
    public void setSessionID(String sessionID) {
        this.sessionID = sessionID;
    }

    public Event withSessionID(String sessionID) {
        this.sessionID = sessionID;
        return this;
    }

    /**
     * 
     * (Required)
     * 
     * @return
     *     The eventType
     */
    public String getEventType() {
        return eventType;
    }

    /**
     * 
     * (Required)
     * 
     * @param eventType
     *     The eventType
     */
    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public Event withEventType(String eventType) {
        this.eventType = eventType;
        return this;
    }

    
    public Map<String, String> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperties (Map<String, String> additonalProperties) {
    	this.additionalProperties = additonalProperties;
    }
    
    @Override
    public String toString() {
    	
    	StringWriter toReturn =  new StringWriter();
    	ObjectMapper mapper = new ObjectMapper();
    	try {
			mapper.writeValue(toReturn, this);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
    	return toReturn.toString();    	
    }
    
//    public void setAdditionalProperty(String name, String value) {
//        this.additionalProperties.add(new PropertyValue(name, value));
//    }
//
//    public Event withAdditionalProperty(String name, String value) {
//        this.additionalProperties.add(new PropertyValue(name, value));
//        return this;
//    }

}
