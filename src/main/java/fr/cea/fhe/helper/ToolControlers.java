package fr.cea.fhe.helper;

import java.io.File;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.Base64;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.Vector;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.zip.CheckedInputStream;

import javax.net.ssl.SSLContext;

import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import fr.cea.fhe.ApplicationDeployer;
import fr.cea.fhe.helper.Constants.AnalysisMethodeEnum;
import fr.cea.fhe.models.KeyObject;
import fr.cea.fhe.models.KeyObject.KeyInfoEnum;
import fr.cea.fhe.repository.FheKeyRepository;
import fr.cea.fhe.restapi.FheServiceImplementation;
import fr.cea.fhe.models.KeyTransfer;
import fr.cea.fhe.services.EncryptBlackListServices;
import fr.cea.fhe.type.RestResponse;
import fr.cea.fhe.type.xacml.RequestAttributes;
import fr.cea.fhe.type.xacml.RequestContainer;
import fr.cea.fhe.type.xacml.RequestElement;
import fr.cea.kemanager.lib.DataObject;

public class ToolControlers {
	private static final Logger log = LoggerFactory.getLogger(ToolControlers.class);

	
	public static byte[] GetHEPublicKey(List<KeyObject> lstKeys)
	{
		for(KeyObject keyO : lstKeys)
		{
			if(keyO.getKeyInfo().compareTo(KeyInfoEnum.FHE_PK) == 0)
				return keyO.getKey();
		}
		return null;
	}
	
	public static byte[] GetHEEvaluationKey(List<KeyObject> lstKeys)
	{
		for(KeyObject keyO : lstKeys)
		{
			if(keyO.getKeyInfo().compareTo(KeyInfoEnum.FHE_EVK) == 0)
				return keyO.getKey();
		}
		return null;
	}
	
	public static byte[] GetHEParameter(List<KeyObject> lstKeys)
	{
		for(KeyObject keyO : lstKeys)
		{
			if(keyO.getKeyInfo().compareTo(KeyInfoEnum.FHE_PARAM) == 0)
				return keyO.getKey();
		}
		return null;
	}
		
	
	public static DataObject FheEncryptingDataBase(
			ExecutorService executor,			
			KeyTransfer lstKeys,
			String baseDir,
			String dsAId, 
			String FHEModel,
			boolean IsInvokedFromControler) throws IOException {
		
		assert(lstKeys.getLstKeys().size() != 0);
				      
		
		DataObject reServerResponse = new DataObject().dataContent("PROCESSING");
					
		FileOperations.PrintData(baseDir + File.separator + Constants.FheKeys.FHE_PK_FILE, GetHEPublicKey(lstKeys.getLstKeys()));
		FileOperations.PrintData(baseDir + File.separator + Constants.FheKeys.FHE_EVAL_FILE, GetHEEvaluationKey(lstKeys.getLstKeys()));
		FileOperations.PrintData(baseDir + File.separator + Constants.FheKeys.FHE_PARAM_FILE, GetHEParameter(lstKeys.getLstKeys()));		
		log.info("[FHE-BLACKLIST-MANAGER] configuration folder exec : " + Constants.FheScripts.FHE_CONFIG_EXEC + " " + baseDir + " " + FHEModel);
		BashTools.Exec(true, Constants.FheScripts.FHE_CONFIG_EXEC, baseDir, FHEModel);
		
		log.info("[FHE-BLACKLIST-MANAGER] Starting exec creating Blacklist Database for " + dsAId);		
		if(IsInvokedFromControler && executor != null)
		{ 	/*in case of preparing database in advance*/
			executor.execute(new EncryptBlackListServices(baseDir, dsAId, FHEModel));
		}
		else 
		{   /*in case of sending request analysis without preparing database in advance*/
			new EncryptBlackListServices(baseDir, dsAId, FHEModel).run();
		}
		log.info("[FHE-BLACKLIST-MANAGER] Creating Blacklist Database for " + dsAId + " Done !");
		return reServerResponse;
	}	
	
	public static boolean checkExistingDataBase(String staticDataBase) {		 
		File folderDataBase = new File(staticDataBase);
		if(folderDataBase.isDirectory() &&  folderDataBase.exists())
		{
			log.info("[FHE-BLACKLIST-MANAGER] Checking folder "  + staticDataBase);
			for (final File fileCiphetext : folderDataBase.listFiles())
			{
				if(fileCiphetext.getName().contains(".ct"))
					return true;
			}
		}
		return false; 
	}	
	
	public static ResponseEntity<String> CreateDatabase(
			ExecutorService executor,
			FheKeyRepository fheKeyController, 
			String DsaID, String AnalysisMethod,
			String databaseDir,
			boolean IsInvokedFromControler) throws Exception {
		log.info("CreateDatabase process : encrypt method start working with database dir =" + databaseDir);
		/**
		 * Checking existing key ?
		 * */
		String requestId = UUID.randomUUID().toString();
		fheKeyController.init(requestId, DsaID, AnalysisMethod);                	
		boolean isCreated = fheKeyController.CreatingFHEKeyIfNeed();
		if(isCreated)
		{
			/**
	    	 * Existed keys, process to get keys now
	    	 * */
			KeyTransfer lstKeys = fheKeyController.getKeysForAnalysis();    
			assert(lstKeys != null);
			for(KeyObject ko : lstKeys.getLstKeys())
			{
				log.info("Key " + ko.getKeyInfo().name() + " size = " + ko.getKey().length);
			}
			DataObject reServerResponse = new DataObject();
			    					    			
			try {
				reServerResponse = FheEncryptingDataBase(
					executor, lstKeys, databaseDir, DsaID, 
					AnalysisMethod, IsInvokedFromControler);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
	        	return new ResponseEntity<String>("ERROR: please checking from log service ", HttpStatus.INTERNAL_SERVER_ERROR);                 	                	    
			}
		}                	            		       	   
		log.info("Finished for preparation before sending");
		return new ResponseEntity<String>( "{\"response\": \"DATABASE CREATED !\"}", HttpStatus.CREATED);   
	}
	
	public static ResponseEntity<String> CreateDatabase(
			ExecutorService executor,
			FheKeyRepository fheKeyController, 
			String DsaID, String AnalysisMethod, 
			boolean IsInvokedFromControler) throws Exception {		
		String baseDir = String.format(Constants.FheAlgo.BLACKLIST.PREPAREDATA_DIR, DsaID, AnalysisMethod);
		log.info("CreateDatabase process with auto database dir " + baseDir);
		AnalysisMethodeEnum aModel = AnalysisMethodeEnum.valueOf(AnalysisMethod);
		DataObject reServerResponse = new DataObject();
		if (aModel == null)
		    throw new Exception("No method is available for this analysis");
		
		                	
			if(checkExistingDataBase(baseDir) == true)  
			{
				reServerResponse.dataName("Your request was already submited, please change request ID for invoking again !");
				return new ResponseEntity<String>( "{\"response\": \"DATABASE EXISTED !\"}", HttpStatus.CREATED);                		
			}
			                
			log.info("ResponseEntity<ServerResponse> encrypt method start working");
			/**
			 * Checking existing key ?
			 * */
			String requestId = UUID.randomUUID().toString();
			fheKeyController.init(requestId, DsaID, AnalysisMethod);                	
			boolean isCreated = fheKeyController.CreatingFHEKeyIfNeed();
			if(isCreated)
			{
				/**
		    	 * Existed keys, process to get keys now
		    	 * */
				KeyTransfer lstKeys = fheKeyController.getKeysForAnalysis();    
				assert(lstKeys != null);
				for(KeyObject ko : lstKeys.getLstKeys())
				{
					log.info("Key " + ko.getKeyInfo().name() + " size = " + ko.getKey().length);
				}
				    					    				
				try {
					reServerResponse = FheEncryptingDataBase(
						executor, lstKeys, baseDir, DsaID, 
						AnalysisMethod, IsInvokedFromControler);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
		        	return new ResponseEntity<String>("ERROR: please checking from log service ", HttpStatus.INTERNAL_SERVER_ERROR);                 	                	    
				}
			}                	            		       	   
			log.info("Finished for preparation before sending");
			return new ResponseEntity<String>( "{\"response\": \"DATABASE CREATED !\"}", HttpStatus.CREATED);   	    
		                                                                           
		//return new ResponseEntity<String>("NO SUPPORTED FHE MODEL " + AnalysisMethod, HttpStatus.FORBIDDEN);
	}

	
	public static RestTemplate createSSLIgnoringRestTemplate() {
		TrustStrategy acceptingTrustStrategy = (X509Certificate[] chain, String authType) -> true;

		SSLContext sslContext;
		try {
			sslContext = org.apache.http.ssl.SSLContexts.custom()
			        .loadTrustMaterial(null, acceptingTrustStrategy)
			        .build();
		} catch (KeyManagementException | NoSuchAlgorithmException | KeyStoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}

		SSLConnectionSocketFactory csf = new SSLConnectionSocketFactory(sslContext);

		CloseableHttpClient httpClient = HttpClients.custom()
		        .setSSLSocketFactory(csf)
		        .build();

		HttpComponentsClientHttpRequestFactory requestFactory =
		        new HttpComponentsClientHttpRequestFactory();

		requestFactory.setHttpClient(httpClient);

		RestTemplate restTemplate = new RestTemplate(requestFactory);
		
		return restTemplate;
	}
	
	public static String createDpoObject(String isiDPOApiURL, File fileZip, String m_metadata, String dsaId) 
			throws JsonParseException, JsonMappingException, IOException {
		
		String dposId  = "";				
		///////////////////////////////////////
		//CREATE
		///////////////////////////////////////
		
		
		//URL fileURL = this.getClass().getClassLoader().getResource(INPUT_FILE);
		
		ObjectMapper mapper = new ObjectMapper();
		Map<String, Object> jsonMap = mapper.readValue(m_metadata,
		new TypeReference<Map<String,Object>>(){});
		
		String plainCreds = "user:password";
		byte[] plainCredsBytes = plainCreds.getBytes();
		byte[] base64CredsBytes = Base64.getEncoder().encode(plainCredsBytes);
		String base64Creds = new String(base64CredsBytes);
		
		RequestContainer container = new RequestContainer();
		RequestElement el = new RequestElement();
		RequestAttributes attr = new RequestAttributes();
		attr.setAttributeId("ns:c3isp:dpo-metadata");
		attr.setValue(mapper.writeValueAsString(jsonMap));
		
		
		RequestAttributes dsaIdAttr = new RequestAttributes();
		dsaIdAttr.setAttributeId("ns:c3isp:dsa-id");
		dsaIdAttr.setValue(dsaId);
		
		LinkedList<RequestAttributes> list = new LinkedList<RequestAttributes>();
		list.add(attr);
		el.setAttributes(list);
		container.setRequest(el);		
		
		MultiValueMap<String, Object> parts = new LinkedMultiValueMap<String, Object>();
		
		parts.add("input_metadata", container);
		parts.add("fileToSubmit", new FileSystemResource(fileZip));
		
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", "Basic " + base64Creds);
		
		headers.setContentType(MediaType.MULTIPART_FORM_DATA);
		HttpEntity<MultiValueMap<String, Object>> request = 
		new HttpEntity<MultiValueMap<String, Object>>(parts, headers);		
		
		RestTemplate restTemplate = ToolControlers.createSSLIgnoringRestTemplate();		
		
		ResponseEntity<RestResponse> response = null;
				
		response = restTemplate//.withBasicAuth("user", "password")
					.exchange(isiDPOApiURL, HttpMethod.POST, request, RestResponse.class);		
		if(response.getStatusCode() == HttpStatus.OK)
		{
			RestResponse respBody = response.getBody();			
			dposId = respBody.getAdditionalProperties().get("dposId");			
			log.info("dpo_id is: " + dposId);
			return dposId ;
		}								
		return "";
	}
}
