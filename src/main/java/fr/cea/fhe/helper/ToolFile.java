package fr.cea.fhe.helper;

import java.io.*;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

import org.springframework.stereotype.Component;

import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component
public class ToolFile {
	final static Logger logger = LoggerFactory.getLogger( ToolFile.class );
		
	private String baliseContentFile; 

	public void setBaliseContentFile(String baliseContentFile) {
		this.baliseContentFile = baliseContentFile;
	}	
	
	public boolean isNullOrEmpty(String pathToFile)
	{
		if(pathToFile == null || pathToFile.length() == 0) return true;
		
		return false;
	}

	/**
	 * /a/b/c.txt --> true <br>
	 * /a/b/c --> false <br>
	 * */
	public boolean isContainedFileName(String hdfsPathToStoreFileName) {		
		String extension = FilenameUtils.getExtension(hdfsPathToStoreFileName);		
		if(extension != null && extension.length() > 0)
			return true;
		return false;
	}
	
	/**
	 * /a/b/c.txt --> /a/b <br>
	 * /a/b/c --> /a/b/c <br>
	 * */
	public String pathFormater(String hdfsPathToStoreWithFileName) {
		if(isContainedFileName(hdfsPathToStoreWithFileName)) 
		{
			String fileName = FilenameUtils.getName(hdfsPathToStoreWithFileName);
			return hdfsPathToStoreWithFileName.replace(File.separator + fileName, "");
		}
		return hdfsPathToStoreWithFileName;
	}
	
	/**
	 * a/b/c.txt --> c.txt <br>
	 * a.txt     --> a.txt <br>
	 * a/b/c     --> c <br>
	 * a/b/c/    --> "" <br>
	 * */
	public String getFileName(String pathToFile)
	{
		if(isContainedFileName(pathToFile) == false) return "";
		return FilenameUtils.getName(pathToFile);		
	}	

	/**
	 * a/b/c.txt --> b <br>
	 * a.txt     --> / <br>
	 * a/b/c     --> c <br>
	 * a/b/c/    --> c <br>
	 * Used for Ingestion Data Layer, to get same current folder name and change just next ingestion step
	 * oldIngesSetp/FolderName/file.ext --> newIngesSetp/FolderName/file.ext  
	 * @param currentWorkingHdfsFolder hdfs://path/to/current/folder
	 * @return FolderName
	 * */
	public String getCurrentWorkingFolder(String currentWorkingHdfsFolder) {
		
		String currentFolder = currentWorkingHdfsFolder;
		if(currentWorkingHdfsFolder.endsWith("/"))
			currentFolder = currentWorkingHdfsFolder.substring(0, currentWorkingHdfsFolder.length() - 1); 
		if(currentFolder.length() == 0)
			return "/";
		/*a/b/c  -> c*/
		currentFolder = pathFormater(currentFolder); 
		String parentFolder = FilenameUtils.getPathNoEndSeparator(currentFolder);
		
		return currentFolder.replace(parentFolder + File.separator, "");
	}
	
	public String encodeContentToBase64(byte[] contentFile)
	{
		return Base64.getEncoder().encodeToString(contentFile);
	}
	
	public String decodeContentFromBase64(byte[] contentFile)
	{
		return new String(Base64.getDecoder().decode(contentFile), StandardCharsets.UTF_8);
	}
	
	/**
	 * contentFile => < baliseContentFile >contentFile < /baliseContentFile >
	 * */
	public String contentFormated(String contentFileBase64)
	{
		return String.format("<%s>%s</%s>", baliseContentFile, contentFileBase64, baliseContentFile);
	}
	
	/**ContentFile in Datalake
	 * --lVy4TQpdZfIUpxOPH3riPy7t0ze9qMz9OPX <br>
	 * Content-Disposition: form-data; name="file"; filename="origineFileName.data" <br>
	 * Content-Type: application/octet-stream <br>
	 * Content-Length: 0 <br>
	 * here is<br>
	 * my content in base 64 <br>
	 * --lVy4TQpdZfIUpxOPH3riPy7t0ze9qMz9OPX-- <br>
	 * => ContentFile : here is<br>
	 * my content in base 64 <br>
	 * */
	public String fileFormaterConvertFromDataLake(String contentFileBase64)
	{		
		if(baliseContentFile == null || baliseContentFile.length() == 0)
		{
			throw new ExceptionInInitializerError("Please correctly define the hadoop balise !");
		}
			
		String contentFile = contentFileBase64; 			
		String baliseOpenContent = String.format("<%s>", baliseContentFile);
		String baliseCloseContent = String.format("</%s>", baliseContentFile);
		
		int indexOpen = contentFileBase64.indexOf(baliseOpenContent);
		boolean isMalformated = false;
		if(indexOpen >= 0)
		{
			int indexClose = contentFileBase64.indexOf(baliseCloseContent);
			if(indexClose > 0)
			{
				contentFile = contentFileBase64.substring(indexOpen, indexClose);
			}
			else isMalformated = true;
		}
		else isMalformated = true;
		
		if(isMalformated){
			throw new ExceptionInInitializerError("The content was not correctly formated !");
		}
		
		if(contentFile.contains(baliseOpenContent))
		{
			contentFile = contentFile.replace(baliseOpenContent, ""); 
		}
		if(contentFile.contains(baliseCloseContent))
		{
			contentFile = contentFile.replace(baliseCloseContent, ""); 
		}
		return contentFile;
	}

}
