/**
 *  Copyright 2017 Hewlett Packard Enterprise Development Company, L.P.
 */
package fr.cea.fhe.restapi;

import java.io.File;
import java.util.concurrent.Executors;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.cea.fhe.ApplicationDeployer;
import fr.cea.fhe.helper.Constants;
import fr.cea.fhe.helper.Constants.AnalysisMethodeEnum;
import fr.cea.fhe.helper.ToolControlers;
import fr.cea.fhe.models.FHEResponse;
import fr.cea.fhe.models.FheAnalysisModel;
import fr.cea.fhe.repository.FheKeyRepository;
import fr.cea.fhe.services.*;

import fr.cea.kemanager.lib.DataObject;
import io.swagger.annotations.ApiParam;

@Controller
public class FheServiceImplementation implements FheServiceApi{
	
	private static final Logger log = LoggerFactory.getLogger(FheServiceImplementation.class);

	/**
	 * These two are examples of configuration extracted from the
	 * application.properties file under the src/main/resources. Springboot will
	 * automatically load the file and its values, if file is not in the local
	 * path, it can be in the java classpath or set as an environment variable
	 */
	@Value("${setting1}")
	private String setting1;

	@Value("${iai.fhe.prefixVDL}")
	private String prefixVDL;
	
	@Value("${rest.endpoint.url.callGet}")
	private String callGetEndpoint;

	@Value("${rest.endpoint.url.callPostISI}")
	private String callPostISIEndpoint;

	
	@Value("${security.user.name}")
	private String restUser;
	@Value("${security.user.password}")
	private String restPassword;
	
	@Autowired
	private FheKeyRepository fheKeyController;

	private int threadPoolCount = 8;
	private final HttpServletRequest request;
	
	private final String logStatus = "[FHE-BLACKLIST-MANAGER]: ";
	
	@Autowired
	private FheAnalysisServices fheAnalysisServices; 
	
	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}

	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder restTemplateBuilder) {
		log.info("Setting up basic authorizations.");
		return restTemplateBuilder.basicAuthorization(restUser, restPassword).build();
	}


	@Autowired
	public FheServiceImplementation(ObjectMapper objectMapper, HttpServletRequest request) {
	//	this.objectMapper = objectMapper;
		this.request = request;		
	}	
	
	
	@Override
	public ResponseEntity<String> checkBelongToBlackList (	
			@PathVariable("DsaID") String DsaID,
			@ApiParam(value = "",required=true, allowableValues = FHE_ALGORITHM) @PathVariable("AnalysisMethod") String AnalysisMethod,			
			@ApiParam(value = "Send encrypted IP to analysis" ,required=true )  @Valid @RequestBody DataObject fileData ) 
	{
		String accept = request.getHeader("Accept");		
        if (accept != null && accept.contains("application/json")) {
            try {
    			String requestId = fileData.getDataName();
    			log.info("Processing with FHE analysis with requestID " + requestId);
            	if(ApplicationDeployer.executor.isShutdown()) 
            		ApplicationDeployer.executor = Executors.newFixedThreadPool(threadPoolCount);
            	            	                
            	AnalysisMethodeEnum aModel = AnalysisMethodeEnum.valueOf(AnalysisMethod);        		
        		if (aModel == null)
        			return new ResponseEntity<String>("NO SUPPORTED FHE MODEL " + AnalysisMethod, HttpStatus.FORBIDDEN);
        		            
            	String baseWorkingDir = String.format(Constants.FheAlgo.BLACKLIST.PREPAREDATA_DIR, DsaID, AnalysisMethod);
            	log.info(logStatus + "init checkBelongToBlackList over dir ... " + baseWorkingDir);                	
            	fheKeyController.init(requestId, DsaID, AnalysisMethod);
                BlackListAnalysisServices blService = new BlackListAnalysisServices(
                		fheKeyController, AnalysisMethod, 
                		baseWorkingDir, DsaID, requestId, fileData, callPostISIEndpoint);
                
            	/*init for get FHE key now*/                    
                String baseWorkingRequestFolder = baseWorkingDir + File.separator + requestId;
                blService.saveFHEKeysForAnalysis(baseWorkingRequestFolder);
            	ApplicationDeployer.executor.execute(blService);
            	log.info(logStatus + "checkBelongToBlackList REPLY NOW FOR REQUEST : " + requestId);  
            	return new ResponseEntity<String>( "{\"response\": \"ANALYZING NOW !\"}", HttpStatus.PROCESSING);     
            	
            } catch (Exception e) {
                log.error(logStatus + "Couldn't serialize response for content type application/json", e);
                e.printStackTrace();
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }        
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);        
	}
	
	@Override
	public ResponseEntity<FHEResponse> checkBelongToBlackListWithVDL(
    		@ApiParam(value = "",required=true, allowableValues = FHE_ALGORITHM) @PathVariable("AnalysisMethod") String AnalysisMethod,        		
    		@ApiParam(value = "The VDL path to data")  @Valid @RequestParam(value = "vdlUrl", required = true) String vdlDposIDUrl)
	{
		/**
		 * The request is with this format
		 * request=https://iaic3isp.iit.cnr.it/fhe-conn-malicious-host/v1/analyseWithVDL/BLACK_LIST_FULL
		 * ?vdlUrl\=file:///opt/isi/datalakebuffer/21b0f6f5-44a4-41cc-9a95-153940b89e7f 
		 * */		
		try {            	
        	if(vdlDposIDUrl == null || vdlDposIDUrl.length() == 0)
        		throw new ExceptionInInitializerError("Link to Virtual data lake is not defined !");
        	
        	log.info(logStatus + "Invoke processing IPv4 black list in virtual data lake : "+ vdlDposIDUrl 
        			+" with FHE analysis method " + AnalysisMethod);
        	String pathToVDL = "";
        	if(vdlDposIDUrl.contains(prefixVDL))
        	{
        		pathToVDL = vdlDposIDUrl.replace(prefixVDL, "");
        	}
        	/*
        	 * 1 - get DSA ID from dsa.ini 
        	 * 2 - start to analysis and get model to analysis
        	 * */        	
        	FheAnalysisModel fheModel = FheAnalysisModel.fromValue(AnalysisMethod);
        	log.info(logStatus + " fheModel = " + fheModel + " pathVDL = " + pathToVDL);
        	FHEResponse objDPOs = fheAnalysisServices.checkBelongToBlackListWithVDL(fheModel, pathToVDL);
        	log.info(logStatus + "Response from FHE - Analysis BLACK_LIST : " + objDPOs.toString());
        	
        	return new ResponseEntity<FHEResponse>(objDPOs, HttpStatus.ACCEPTED);
        } catch (Exception e) {
            log.error(logStatus + "Couldn't handle request for error : ", e);
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);            
        }
	}

	@Override
	public ResponseEntity<String> prepareDataBaseBlackList(
			@ApiParam(value = "",required=true) @PathVariable("DsaID") 			String DsaID,
    		@ApiParam(value = "",required=true, allowableValues = FHE_ALGORITHM) 
						@PathVariable("AnalysisMethod") String AnalysisMethod) 
	{
		String accept = request.getHeader("Accept");		
        if (accept != null && accept.contains("*")) {
            try {    		
    			log.info(logStatus + "Prepare IPv4 Database with FHE encryption for " + DsaID);
            	if(ApplicationDeployer.executor.isShutdown()) 
            		ApplicationDeployer.executor = Executors.newFixedThreadPool(threadPoolCount);
            	
            	return ToolControlers.CreateDatabase(
            			ApplicationDeployer.executor, 
            			fheKeyController, DsaID, AnalysisMethod, true);            	
            } catch (Exception e) {
                log.error(logStatus+  "Couldn't serialize response for content type application/json", e);
                e.printStackTrace();
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }        
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);      
	}	
}
