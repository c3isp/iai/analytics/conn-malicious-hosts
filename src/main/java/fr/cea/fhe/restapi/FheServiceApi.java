package fr.cea.fhe.restapi;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.cea.fhe.models.FHEResponse;
import fr.cea.kemanager.lib.DataObject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-03-02T14:12:37.705Z")

@Api(value = "fhe", description = "the FHE API")
@RequestMapping("/v1")
public interface FheServiceApi {
	
	/**
	 * Need to be compatible with FheAnalysisModel class
	 */
	static final String FHE_ALGORITHM = "BLACK_LIST_FULL, BLACK_LIST_LOW, BLACK_LIST_MEDIUM, BLACK_LIST_HIGH"; 
	
    default Optional<ObjectMapper> getObjectMapper() {
        return Optional.empty();
    }

    default Optional<HttpServletRequest> getRequest() {
        return Optional.empty();
    }

    default Optional<String> getAcceptHeader() {
        return getRequest().map(r -> r.getHeader("Accept"));
    }

	@ApiOperation(value = "processing 32 Fhe encrypted bits with FHE analysis for detecting whether it is malicious IP", 
			nickname = "checkBlackList", 
			notes = "Receiving a list of ciphertext which is Fhe encrypted IPv4 as input, this method invoke Fhe analysis detect whether it belongs to a blacklist. "
					+ "And storing analysis result into DPO database, send back to client the corresponding DPO-ID if URL was define in otherProperties with key url"
					+ "", 
			response = String.class, 
    		authorizations = {
            //@Authorization(value = "basicAuth")
        }, tags={ "black list", })
        @ApiResponses(value = { 
            @ApiResponse(code = 202, message = "Accepted request", response = String.class),
            @ApiResponse(code = 400, message = "Bad request", response = String.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = String.class),            
            @ApiResponse(code = 404, message = "Not found", response = String.class),
            @ApiResponse(code = 500, message = "Error for HTTPS call trustAnchors", response = String.class)})
        @RequestMapping(value = "/analyse/{AnalysisMethod}/{DsaID}",
    		consumes = { "application/json" },
            produces = { "application/json" },             
            method = RequestMethod.POST)
        ResponseEntity<String> checkBelongToBlackList(
        		@PathVariable("DsaID") 			String DsaID,
        		@ApiParam(value = "",required=true, allowableValues = FHE_ALGORITHM) @PathVariable("AnalysisMethod") String AnalysisMethod,
    			@ApiParam(value = "The encrypted Bundle content to upload." ,required=true )  @Valid @RequestBody DataObject fileData );
	
	@ApiOperation(value = "black list processing with FHE IPs corresponding to serveral DPO-IDs that are stored in Virtual Data Lake", 
			nickname = "checkBelongToBlackListWithVDL", 
			notes = "Paramater inputs are Analysis Method, Virtual Datalake Path containing FHE DPO-IDs."
					+ "Each IP encrypted in FHE format is 32 ciphertexts, which are stored in one ZIP file corresponding to 1 DPO-ID"
					+ "DSA-ID is stored in DPO bundle. The analysis results for each IP will be packed into 1 zip file and send to DPOS for storing, "
					+ "then send back this DPO-ID"
					+ "", 			
			response = String.class, 
    		authorizations = {
            //@Authorization(value = "basicAuth")
        }, tags={ "black list", })
        @ApiResponses(value = { 
            @ApiResponse(code = 202, message = "Accepted request", response = FHEResponse.class),
            @ApiResponse(code = 400, message = "Bad request", response = String.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = String.class),            
            @ApiResponse(code = 404, message = "Not found", response = String.class),
            @ApiResponse(code = 500, message = "Error for HTTPS call trustAnchors", response = String.class)})
        @RequestMapping(value = "/analyseWithVDL/{AnalysisMethod}",
    		consumes = { "application/json" },
            produces = { "application/json" },
            method = RequestMethod.POST)
        ResponseEntity<FHEResponse> checkBelongToBlackListWithVDL(        		
        		@ApiParam(value = "",required=true, allowableValues = FHE_ALGORITHM) @PathVariable("AnalysisMethod") String AnalysisMethod,        		
    			@ApiParam(value = "The VDL path to data")  @Valid @RequestParam(value = "vdlUrl", required = true) String vdlDposIDUrl );
	
	@ApiOperation(value = "Prepare Database for IPv4 blacklist.", nickname = "prepareDataBaseBlackList", 
			notes = "encrypting IPv4 list with FHE encryption", 
			response = String.class, 
    		authorizations = {
        }, tags={ "black list", })
        @ApiResponses(value = { 
            @ApiResponse(code = 200, message = "Server response", response = String.class),
            @ApiResponse(code = 400, message = "Bad request", response = String.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = String.class),            
            @ApiResponse(code = 404, message = "Not found", response = String.class),
            @ApiResponse(code = 500, message = "Error for HTTPS call trustAnchors", response = String.class)})
        @RequestMapping(value = "/database/{AnalysisMethod}/{DsaID}",
    		consumes = { "*" },
            produces = { "*" },             
            method = RequestMethod.GET)
        ResponseEntity<String> prepareDataBaseBlackList(
        		@ApiParam(value = "",required=true) @PathVariable("DsaID") 			String DsaID,
        		@ApiParam(value = "",required=true, allowableValues = FHE_ALGORITHM) @PathVariable("AnalysisMethod") String AnalysisMethod);
}
