package fr.cea.fhe.models;

import java.util.ArrayList;

public class DposArrObj {
	
	public DposArrObj() {
		success = false;
		arrElements = new ArrayList<>();  
	} 
	
	public DposArrObj(boolean message, ArrayList<String> dposList) {
		super();
		this.success = message;
		this.arrElements = dposList;
	}
	
	public boolean isSuccess() {
		return success;
	}

	public DposArrObj success(boolean success) {
		this.success = success;
		return this;
	}
	
	public DposArrObj arrElements(ArrayList<String>  new_elements) {
		this.arrElements = new_elements;
		return this;
	}
	
	public void setSuccess(boolean success) {
		this.success = success;
	}

	public ArrayList<String> getArrElements() {
		return arrElements;
	}

	public void setArrElements(ArrayList<String> arrElements) {
		this.arrElements = arrElements;
	}	

	private boolean success; 
	private ArrayList<String>  arrElements;
	
		
}
