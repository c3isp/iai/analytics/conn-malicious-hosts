package fr.cea.fhe.models;

import java.io.IOException;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

public class FheMetadata {
	
	public FheMetadata() {		
		super();
		this.requestId = UUID.randomUUID().toString();
		this.fheData = true;
		this.bitCount = 1;
	}
	
	@JsonProperty("dsa_id")
	private String dsaId;
	
	@JsonProperty("fhe_analysis_model")
	private String fheAnalysisMethod;
	
	@JsonProperty("request_id")
	private String requestId;
	
	@JsonProperty("partial_data")
	private String partialData;
		
	@JsonProperty("fhe_data")
	@JsonSerialize(using=BooleanSerializer.class)
    @JsonDeserialize(using=BooleanDeserializer.class)
	private boolean fheData;
	
	@JsonProperty("bit_count")
	private int bitCount; 
	
	public String getPartialData() {
		return partialData;
	}

	public void setPartialData(String partialData) {
		this.partialData = partialData;
	}
	
	public FheMetadata partialData(String partialData) {
		this.partialData = partialData;
		return this;
	}
	
	public String getDsaId() {
		return dsaId;
	}
	
	public void setDsaId(String dsaId) {
		this.dsaId = dsaId;
	}
	
	public FheMetadata dsaId(String dsaId) {
		this.dsaId = dsaId;
		return this;
	}
	
	public String getFheAnalysisMethod() {
		return fheAnalysisMethod;
	}
	
	public FheMetadata fheAnalysisMethod(String fheAnalysisMethod) {
		this.fheAnalysisMethod = fheAnalysisMethod;
		return this;
	}
	
	public void setFheAnalysisMethod(String fheAnalysisMethod) {
		this.fheAnalysisMethod = fheAnalysisMethod;
	}

	/**
	 * @return the bitCount
	 */
	public int getBitCount() {
		return bitCount;
	}

	/**
	 * @param bitCount the bitCount to set
	 */
	public void setBitCount(int bitCount) {
		this.bitCount = bitCount;
	}
	
	public FheMetadata bitCount(int bitCount) {
		this.bitCount = bitCount;
		return this;
	}
	
}

class BooleanSerializer extends JsonSerializer<Boolean> {

    @Override
    public void serialize(Boolean bool, JsonGenerator generator, SerializerProvider provider) throws IOException, JsonProcessingException {
        generator.writeString(bool ? "true" : "false");
    }   
}

class BooleanDeserializer extends JsonDeserializer<Boolean> {

    @Override
    public Boolean deserialize(JsonParser parser, DeserializationContext context) throws IOException, JsonProcessingException {
        return new String("true").equals(parser.getText()) == true;
    }
}
