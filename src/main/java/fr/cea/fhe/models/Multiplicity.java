package fr.cea.fhe.models;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class Multiplicity {
	private String ip;
	private Integer listSize;
	public Multiplicity(String ip, Integer listSize) {
		super();
		this.ip = ip;
		this.listSize = listSize;
	}
	public Multiplicity() {
		this.ip = "192.168.0.1";
		this.listSize = 10;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public Integer getListSize() {
		return listSize;
	}
	public void setListSize(Integer listSize) {
		this.listSize = listSize;
	}

	
	
	public static String multiplicity(String[] argv) throws Exception {  
		List<String> cmd = new ArrayList<String> () ; 
		cmd.add("/home/hcatworks/fhe/scripts/multiplicity.sh");
		cmd.add(argv[0]); // one IPv4	
		cmd.add(argv[1]); // list size

		if (argv[2] != null)
		{
			cmd.add(argv[2]); // list name (default value "ipv4.dat")
		}
		ProcessBuilder builder = new ProcessBuilder(cmd);
		builder.redirectErrorStream(true);
		final Process process = builder.start();
		InputStream is = process.getInputStream();
		InputStreamReader isr = new InputStreamReader(is);
		BufferedReader br = new BufferedReader(isr);
		String line;
		String last_line="";
		while ((line = br.readLine()) != null) {
			last_line = line;
			System.out.println(line);
		}
		return last_line;
	}
}
