package fr.cea.fhe.models;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FHEResponse {

	@JsonProperty("fheResults")
	private List<FHEResults> fheResults;
	@JsonProperty("sessionId")
	private String sessionId;

	public FHEResponse() {
		fheResults  = new ArrayList<FHEResults>();
	}

	public List<FHEResults> getFheResults() {
		return fheResults;
	}

	public void setFheResults(List<FHEResults> fheResults) {
		this.fheResults = fheResults;
	}
	
	public void addFheResult(FHEResults fheResult)
	{
		this.fheResults.add(fheResult);
	}
	
	public FHEResponse fheResults(List<FHEResults> fheResults) {
		this.fheResults = fheResults;
		return this;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	
	public FHEResponse sessionId(String sessionId) {
		this.sessionId = sessionId;
		return this;
	}

	@Override
	public String toString() {
		return "FHEResponse [fheResults=" + fheResults + ", sessionId=" + sessionId + "]";
	}

}
