package fr.cea.fhe.models;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class Intersection {
	private Integer firstListSize;
	private Integer secondListSize;
	
	public Intersection(Integer firstListSize, Integer secondListSize) {
		super();
		this.firstListSize = firstListSize;
		this.secondListSize = secondListSize;
	}
	public Intersection() {
		this.firstListSize = 10;
		this.secondListSize = 10;
	}

	public Integer getFirstListSize() {
		return firstListSize;
	}
	public void setFirstListSize(Integer firstListSize) {
		this.firstListSize = firstListSize;	
	}
	
	public Integer getSecondListSize() {
		return secondListSize;
	}
	public void setSecondListSize(Integer secondListSize) {
		this.secondListSize = secondListSize;
	}


	
	
	public static String intersection(String[] argv) throws Exception {  
		List<String> cmd = new ArrayList<String> () ; 
		cmd.add("/home/hcatworks/fhe/scripts/intersection.sh");
		cmd.add(argv[0]); // first listsize	
		cmd.add(argv[1]); // second listsize
		if (argv[2] != null)
		{
			cmd.add(argv[2]); // list name (default value "ipv4.dat")
		}
		if (argv[2] != null)
		{
			cmd.add(argv[3]); // list name (default value "ipv4-2.dat")
		}
		ProcessBuilder builder = new ProcessBuilder(cmd);
		builder.redirectErrorStream(true);
		final Process process = builder.start();
		InputStream is = process.getInputStream();
		InputStreamReader isr = new InputStreamReader(is);
		BufferedReader br = new BufferedReader(isr);
		String line;
		String last_line="";
		while ((line = br.readLine()) != null) {
			last_line = line;
			System.out.println(line);
		}
		return last_line;
	}
}
