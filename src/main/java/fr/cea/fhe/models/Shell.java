package fr.cea.fhe.models;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;


public class Shell {
  public static void shell(String[] argv) throws Exception {   
	List<String> cmd = new ArrayList<String> () ; 
    cmd.add("/bin/sh"); 
    cmd.add("-c"); 
    cmd.add(argv[0]);
    cmd.add(argv[1]);
    cmd.add(argv[2]);
    ProcessBuilder builder = new ProcessBuilder(cmd);
    final Process process = builder.start();
    InputStream is = process.getInputStream();
    InputStreamReader isr = new InputStreamReader(is);
    process.waitFor(); 
    BufferedReader br = new BufferedReader(isr);
    String line;
    while ((line = br.readLine()) != null) {
      System.out.println(line);
    }
    System.out.println("Program terminated!");
  }
}	





