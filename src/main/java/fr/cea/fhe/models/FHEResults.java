package fr.cea.fhe.models;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FHEResults {

	@JsonProperty("dpoIp")
	private String dpoIp;
	@JsonProperty("dpoResult")
	private String dpoResult;

	public FHEResults() {
	}

	public String getDpoIp() {
		return dpoIp;
	}

	public void setDpoIp(String dpoIp) {
		this.dpoIp = dpoIp;
	}
	
	public FHEResults dpoIp(String dpoIp) {
		this.dpoIp = dpoIp;	
		return this;
	}

	public String getDpoResult() {
		return dpoResult;
	}

	public void setDpoResult(String dpoResult) {
		this.dpoResult = dpoResult;
	}
	
	public FHEResults dpoResult(String dpoResult) {
		this.dpoResult = dpoResult;	
		return this;
	}

}
