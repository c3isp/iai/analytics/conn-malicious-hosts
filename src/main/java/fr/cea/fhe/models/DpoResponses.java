package fr.cea.fhe.models;

import java.util.HashMap;
import java.util.Map.Entry;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DpoResponses {
	
	public DpoResponses()
	{
		this.additionalProperties = new HashMap<>();
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	
	public DpoResponses sessionId(String sessionId) {
		this.sessionId = sessionId;
		return this;
	}

	public HashMap<String, String> getAdditionalProperties() {
		return additionalProperties;
	}

	public void setAdditionalProperties(HashMap<String, String> additionalProperties) {
		this.additionalProperties = additionalProperties;
	}
	
	public DpoResponses putAdditionalProperties(String key, String value) {
		this.additionalProperties.put(key, value);
		return this;
	}
	
	public DpoResponses additionalProperties(HashMap<String, String> additionalProperties) {
		this.additionalProperties = additionalProperties;
		return this;
	}

	@JsonProperty("sessionId")
	private String sessionId;

	@JsonProperty("additionalProperties")
	private HashMap<String, String> additionalProperties;
	
	@Override
	  public String toString() {
	    StringBuilder sb = new StringBuilder();
	    sb.append("class DpoResponses {\n");
	    
	    sb.append("    sessionId: ").append(toIndentedString(sessionId)).append("\n");
	    sb.append("    additionalProperties: {").append("\n");
	    for ( Entry<String, String> entry : this.additionalProperties.entrySet()) {
    	sb.append("      " + entry.getKey() + ": ").append(toIndentedString(entry.getValue())).append("\n");
		}
	    sb.append("    }").append("\n");
	    sb.append("}");
	    return sb.toString();
	  }

	  /**
	   * Convert the given object to string with each line indented by 4 spaces
	   * (except the first line).
	   */
	  private String toIndentedString(java.lang.Object o) {
	    if (o == null) {
	      return "null";
	    }
	    return o.toString().replace("\n", "\n    ");
	  }

}
