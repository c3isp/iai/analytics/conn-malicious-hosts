package fr.cea.fhe.models;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import fr.cea.kemanager.encrypt.models.EncryptObjectRequest;

public class MaliciousResponseModel {

	
	public MaliciousResponseModel(String requestID, String dsaID, String analyseMethod) {
		super();
		this.requestID = requestID;
		this.dsaID = dsaID;
		this.analyseMethod = analyseMethod;		
	}

	public String getRequestID() {
		return requestID;
	}

	public void setRequestID(String requestID) {
		this.requestID = requestID;
	}

	public String getDsaID() {
		return dsaID;
	}

	public void setDsaID(String dsaID) {
		this.dsaID = dsaID;
	}

	public String getAnalyseMethod() {
		return analyseMethod;
	}

	public void setAnalyseMethod(String analyseMethod) {
		this.analyseMethod = analyseMethod;
	}

	public List<EncryptObjectRequest> getLstEncryptObjectRequest() {
		return lstEncryptObjectRequest;
	}

	public void setLstEncryptObjectRequest(List<EncryptObjectRequest> lstEncryptObjectRequest) {
		this.lstEncryptObjectRequest = lstEncryptObjectRequest;
	}

	@JsonProperty("requestID")
	private String requestID = null; 
	
	@JsonProperty("dsaID")
	private String dsaID = null; 
	
	@JsonProperty("analyseMethod")
	private String analyseMethod= null; 
	
	@JsonProperty("lstEncryptObjectRequest")
	private List<EncryptObjectRequest> lstEncryptObjectRequest = null;
	
	public MaliciousResponseModel() {
		
	}
}
