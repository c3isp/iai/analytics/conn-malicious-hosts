package fr.cea.fhe.models;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonValue;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

import com.fasterxml.jackson.annotation.JsonCreator;

/**
 * Gets or Sets FheAnalysisModel
 */
public enum FheAnalysisModel {
  
  FULL("BLACK_LIST_FULL"),
  
  HIGH("BLACK_LIST_HIGH"),
  
  MEDIUM("BLACK_LIST_MEDIUM"),
  
  LOW("BLACK_LIST_LOW");

  private String value;

  FheAnalysisModel(String value) {
    this.value = value;
  }

  @Override
  @JsonValue
  public String toString() {
    return String.valueOf(value);
  }

  @JsonCreator
  public static FheAnalysisModel fromValue(String text) {
    for (FheAnalysisModel b : FheAnalysisModel.values()) {
      if (String.valueOf(b.value).equals(text)) {
        return b;
      }
    }
    return null;
  }
  
  public boolean equal(FheAnalysisModel model)
  {
	  if(this.value.equals(new String(model.toString())))
		  return true; 
	  return false;
  }
}

