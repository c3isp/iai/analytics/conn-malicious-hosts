package fr.cea.fhe.services;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import fr.cea.fhe.helper.BashTools;
import fr.cea.fhe.helper.Constants;
import fr.cea.fhe.helper.FileOperations;
import fr.cea.fhe.helper.ToolsFolder;
import fr.cea.fhe.models.FheAnalysisModel;

@Service
public class FheWorkers {

	protected Logger log = LoggerFactory.getLogger(FheWorkers.class.getName());	
	private String 				m_staticThreadName = "[FHE-WORKER-";
	private String 				m_threadName = "";
	
	public FheWorkers() {}

	/**
	 * 1 - Copy all database dir in to vdlDposIDUrl and create input/output folder 
	 * 2 - Init configuration for copying the necessary files for invoking FHE
	 * 3 - running analysis and wait the result
	 * */
	public String runAnalysis(String vdlDposIDUrl, String dsaID, FheAnalysisModel AnalysisMethod) {	
		m_threadName = m_staticThreadName + AnalysisMethod.toString() + "]:";
		
		log.info(m_threadName + "START RUNNING FHE at " + vdlDposIDUrl);			
		String dataResultFolderTarget = vdlDposIDUrl + File.separator + Constants.RESULT_ANALYSIS_FOLDER;
		try {
			ToolsFolder.CreateAllFolderNames(dataResultFolderTarget);
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			dataResultFolderTarget = "";
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			dataResultFolderTarget = "";
		}		
		String clearDataPathToFile = vdlDposIDUrl + File.separator + Constants.PARTIAL_DATA_FILE; //to get it from fheMetadata
		String clearData = "";
		if(AnalysisMethod.equal(FheAnalysisModel.FULL) == false && FileOperations.IsExisted(clearDataPathToFile))
		{
			try {
				clearData = new String(FileOperations.GetData(clearDataPathToFile), StandardCharsets.UTF_8);
			} catch (IOException e) {
				e.printStackTrace();
				clearData = "";
			}
		}
		log.info(m_threadName + "configuration folder exec : " + Constants.FheScripts.FHE_CONFIG_EXEC + " " + vdlDposIDUrl + " " + AnalysisMethod);
		BashTools.Exec(true, Constants.FheScripts.FHE_CONFIG_EXEC, vdlDposIDUrl, AnalysisMethod.toString());
		
		log.info(m_threadName + 
	    		String.format("%s %s %s %s", vdlDposIDUrl + File.separator + Constants.FheScripts.FHE_BLACKLIST_ANALYSIS_EXEC, 
	    		dataResultFolderTarget, AnalysisMethod.toString(), clearData));
	    /*Start executing now*/
		BashTools.Exec(true,
				vdlDposIDUrl + File.separator + Constants.FheScripts.FHE_BLACKLIST_ANALYSIS_EXEC, 					
				dataResultFolderTarget, AnalysisMethod.toString(), clearData);
		/*invoke storing DPO-ID for storing result now */			
		log.info(m_threadName + String.format("move result stored at %s to ISI by invoking createDPO-ID",dataResultFolderTarget));
		
		return dataResultFolderTarget.length() == 0 ? 
						vdlDposIDUrl + File.separator + Constants.OUTPUT_ANALYSIS_FOLDER :
						dataResultFolderTarget; 
	}	
}
