package fr.cea.fhe.services;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.validation.constraints.NotNull;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.support.BasicAuthorizationInterceptor;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;


import fr.cea.fhe.helper.BashTools;
import fr.cea.fhe.helper.Constants;
import fr.cea.fhe.repository.FheKeyRepository;
import fr.cea.fhe.repository.IsiapiApiRepository;
import fr.cea.fhe.helper.Event;
import fr.cea.fhe.helper.EventTypes;
import fr.cea.fhe.helper.FileOperations;
import fr.cea.fhe.helper.ToolControlers;
import fr.cea.fhe.helper.ToolsFolder;
import fr.cea.fhe.isi.models.*;
import fr.cea.fhe.models.FheAnalysisModel;
import fr.cea.fhe.models.KeyObject;
import fr.cea.fhe.models.KeyTransfer;
import fr.cea.kemanager.encrypt.models.EncryptObjectRequest;
import fr.cea.kemanager.lib.DataObject;
import fr.cea.kemanager.lib.client.ApiClient;
import fr.cea.kemanager.lib.client.ApiException;

public class BlackListAnalysisServices implements Runnable  {

	private static final Logger log = LoggerFactory.getLogger(BlackListAnalysisServices.class);
	
	private Thread 				m_thread;
	private FheKeyRepository 	m_fheKeyController;
	private DataObject 			m_dataAnalysisObjet;
	private String 				m_threadName = "[FHE - BLACKLIST] ";
	private String 				m_dsaId;
	private String				m_baseDir;
	private String				m_AnalysisMethod;	
	private String 				m_requestID;
	private String 				m_eventNotifyUrl;
	private String 				m_saveToIsiC3ispUrl;
	
	public BlackListAnalysisServices() {
	}
		
	public BlackListAnalysisServices(
			String analysisMethod,
			String baseDir, String dsaId, String requestID, DataObject dataAnalysisObjet) {		
		this.m_dsaId 				= dsaId; 
		this.m_baseDir 				= baseDir;
		this.m_AnalysisMethod 		= analysisMethod;
		this.m_requestID 			= requestID;
		this.m_dataAnalysisObjet 	= dataAnalysisObjet;
	}
	
	public BlackListAnalysisServices(
			FheKeyRepository fheKeyController, 
			String analysisMethod,
			String baseDir, 
			String dsaId, 
			String requestID, 
			DataObject dataAnalysisObjet, 
			String saveToIsiC3isp) {
		this(analysisMethod, baseDir, dsaId, requestID, dataAnalysisObjet);	
		m_fheKeyController = fheKeyController;
		this.m_eventNotifyUrl = dataAnalysisObjet.getUrlToBack();
		/*remove this key for avoiding analyzing over it*/
		this.m_saveToIsiC3ispUrl = saveToIsiC3isp;
	}

	@SuppressWarnings({ "unchecked", "rawtypes", "static-access" })
	@Override
	public void run() {
		/*checking if database was created*/
        if(checkExistingDataBase(        		
        		String.format(Constants.FheAlgo.BLACKLIST.DATABASE_DIR, m_dsaId, m_AnalysisMethod) ) == false)
        {
        	log.info("NO DATA BASE WAS EXISTED ! START CREATING DATABASE for " + m_dsaId);
        	ResponseEntity<String> responseCreateDB;
			try {
				responseCreateDB = ToolControlers.CreateDatabase(null, m_fheKeyController, m_dsaId, m_AnalysisMethod, false);
				if(responseCreateDB.getStatusCode() != HttpStatus.CREATED)
	        	{
	        		throw new Exception ( "{\"Response\": "
	        				+ "\"NO DATA BASE WAS CREATED ! PLEASE MANUALLY CREATE DATABASE FIRST !\"}");
	        	}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}        	
        }                    
		
		String fileLock = m_baseDir + File.separator + Constants.LOCK_FILE;
		String baseWorkingRequestFolder = m_baseDir + File.separator + m_requestID;
		String inputFolderPath = baseWorkingRequestFolder + File.separator + Constants.INPUT_ANALYSIS_FOLDER;
		String inputBkFolderPath = baseWorkingRequestFolder + File.separator + Constants.BACKUP_ANALYSIS_FOLDER;
		//String outputFolderPath = m_baseDir + File.separator + Constants.OUTPUT_ANALYSIS_FOLDER;
		log.info(m_threadName + ": Running for analysis now over folder ... " + baseWorkingRequestFolder);
		try {			
			/*Synchronization : try to avoid overwriting*/
			while(FileOperations.IsExisted(fileLock)) { 
				Thread.currentThread().sleep(1000);			
				log.info(m_threadName + ": Checking lock file  ... ");
			}
			FileOperations.PrintData(fileLock, "1".getBytes());			
			ToolsFolder.CreateAllFolderNames(baseWorkingRequestFolder);
			/*Configuration again*/
		    log.info("[FHE-ENCRYPTION] configuration folder exec : " + Constants.FheScripts.FHE_CONFIG_EXEC 
		    		+ " " + m_baseDir + " " + this.m_AnalysisMethod);
			BashTools.Exec(true, Constants.FheScripts.FHE_CONFIG_EXEC, baseWorkingRequestFolder, this.m_AnalysisMethod);
			
			if (m_thread == null)	m_thread = new Thread (this);		
			
			log.info(m_threadName + ": Starting analysis from request ... " + m_requestID 
					+ " with " + m_dataAnalysisObjet.getOtherProperties().size() + " files");			
			/*Save 32 encrypted bit now to folder input and move BLACK_LIST folder to input folder*/
			Event eventR = null;
			eventR = new Event().withEventType(EventTypes.FHE_ANALYSIS_PROCESS_RESPONSE)
								.withSessionID(m_dsaId);					
					
			Iterator it = m_dataAnalysisObjet.getOtherProperties().entrySet().iterator();
		    while (it.hasNext()) {
		    	Map.Entry<String, String> pair = (Map.Entry)it.next();
		    		       
		        log.debug(m_threadName + pair.getKey() + " = content lenghth " + pair.getValue().length());
		        byte[] content = Base64.getDecoder().decode(pair.getValue().getBytes());
		        String filePath = inputFolderPath + File.separator + pair.getKey();
		        String fileBkPath = inputBkFolderPath + File.separator + pair.getKey();
		        try {
					FileOperations.PrintData(filePath, content);
					/*only active for analysis debug*/
					FileOperations.PrintData(fileBkPath, content);
				} catch (IOException e) {
					e.printStackTrace();
					log.error(m_threadName + pair.getKey() + " = content length " + pair.getValue().length() + " Impossible to save ! ");
					return; 
				}
		        it.remove(); // avoids a ConcurrentModificationException
		    }
		    
		    String clearDataPathToFile = inputFolderPath + File.separator + Constants.PARTIAL_DATA_FILE; //to get it from fheMetadata
			String clearData = "";
			if(FheAnalysisModel.valueOf(m_AnalysisMethod).equal(FheAnalysisModel.FULL) == false 
					&& FileOperations.IsExisted(clearDataPathToFile))
			{
				try {
					clearData = new String(FileOperations.GetData(clearDataPathToFile), StandardCharsets.UTF_8);
				} catch (IOException e) {
					e.printStackTrace();
					clearData = "";
				}
			}
		    
		    String dataResultFolderTarget = String.format(Constants.FheAlgo.BLACKLIST.ANALYSISDATA_DIR, m_dsaId + File.separator + this.m_requestID);
		    /*Create the resultFolder*/
		    ToolsFolder.CreateAllFolderNames(dataResultFolderTarget);
		    /*copy static database into input folder*/
		    String staticDataBase = String.format(Constants.FheAlgo.BLACKLIST.DATABASE_DIR, m_dsaId);		    
		    FileUtils.copyDirectory(FileUtils.getFile(staticDataBase), FileUtils.getFile(inputFolderPath));  		    		 
			
			log.info(m_threadName + 
		    		String.format("%s %s %s %s", 
	    				m_baseDir + File.separator + Constants.FheScripts.FHE_BLACKLIST_ANALYSIS_EXEC, 
	    				dataResultFolderTarget, m_AnalysisMethod, clearData));
		    /*Start executing now*/
			BashTools.Exec(true,
					m_baseDir + File.separator + Constants.FheScripts.FHE_BLACKLIST_ANALYSIS_EXEC, 					
					dataResultFolderTarget, m_AnalysisMethod, clearData);			
			
			/*invoke storing DPO-ID for storing result now */			
			log.info(m_threadName + String.format("post result at %s to ISI for creating DPO-ID",dataResultFolderTarget));
			
			__NotifyAnalysisResult(dataResultFolderTarget, m_eventNotifyUrl);	
			/*clean to save space*/
			log.info("Clean workspace : " + inputFolderPath);
			FileOperations.Shred(inputFolderPath);
		} catch (InterruptedException  | IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} finally {
			try {
				FileOperations.Shred(fileLock);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
		}
	}
	
	
	private void __NotifyAnalysisResult(String dataResultFolderTarget, String m_eventNotifyUrl2) throws IOException {
		File fileResults = null;		
		for(final File res : new File(dataResultFolderTarget).listFiles())
		{
			if(res.isDirectory()) continue;	
			fileResults = res;
			break; //only 1 ciphertext can be send ! // just applied for checking blacklist 
		}
		assert(fileResults != null);
		/*Important here for get content in base64*/
		String contentBase64 = Base64.getEncoder().encodeToString(
				FileOperations.GetData(fileResults.getAbsolutePath()));
		File fTempContentBase64 = ToolsFolder.createTempFile(fileResults.getName(), "ct", contentBase64);
		
		/**
		 * 14:53:47,792 INFO  [fr.cea.fhe.services.BlackListAnalysisServices] (pool-3-thread-1) [FHE - BLACKLIST] post result at /home/nguyen/FHE_CIN/C3ISP/prepareData/DSA_TEST_123456789010-abcdef/366e9c53-3b68-4be4-afc0-1d984edca5d5/analysis to ISI for creating DPO-ID
14:53:47,840 INFO  [fr.cea.fhe.services.BlackListAnalysisServices] (pool-3-thread-1) [FHE - BLACKLIST] send post result to ISI for creating DPO-ID
14:53:48,454 ERROR [stderr] (pool-3-thread-1) fr.cea.kemanager.lib.client.ApiException: Unauthorized
		 * */
		
		log.info(m_threadName + String.format("send post result to ISI for creating DPO-ID",dataResultFolderTarget));
		
		String dpoID = __SendResultFileToISIStorage(fTempContentBase64, false);
		String content = contentBase64;
		if(dpoID.length() > 0)
		{
			log.info(m_threadName + String.format("send back DPO-ID result to client througout %s with data = %s", 
				this.m_eventNotifyUrl), dpoID);
			content = Base64.getEncoder().encodeToString(dpoID.getBytes());
		}
		if(this.m_eventNotifyUrl.length() > 0)
		{
			log.info(m_threadName + String.format("send back to ISI service for storing via URL <"+this.m_eventNotifyUrl+">"));		
			__SendBackToClient(content);
		}
		else 
			log.info(m_threadName + String.format("Don't send back result to local ISI service"));
	}

	private void __SendBackToClient(String contentBase64) {

		RestTemplate restTemplate = new RestTemplate();
		EncryptObjectRequest encryptObjectRequest = new EncryptObjectRequest();
		encryptObjectRequest.binaryObj(contentBase64); 
		encryptObjectRequest.objName(this.m_requestID);
		encryptObjectRequest.setUrlToBack("urlToBack");
		
//		String body = "{\n" + 
//				"\"encryptedContentFile\" : \"contentBase64\"\n" + 
//				"}";
//		HttpHeaders headers = new HttpHeaders();
//		headers.setContentType(MediaType.APPLICATION_JSON);
//
//		HttpEntity<String> entity = new HttpEntity<String>(contentBase64, headers);		
//		restTemplate.getInterceptors().add(
//				  new BasicAuthorizationInterceptor(restUser, restPassword));
		restTemplate.postForEntity(this.m_eventNotifyUrl, encryptObjectRequest, String.class); 
	}

	private String __SendResultFileToISIStorage(File fTempContentBase64, boolean IsActive) throws IOException {		
		/*To do in next step for integration into storing in DPO*/
		if(IsActive == false) return "";
		
		ApiClient apiClient = new ApiClient().setBasePath("https://isic3isp.iit.cnr.it");
		IsiapiApiRepository isiApiController = new IsiapiApiRepository(apiClient);
		String inputMetadata = 	__createRequestISI();	
		
		ISIResponseItem response;
		try {
			response = isiApiController.isiCreateDpo(false, inputMetadata, fTempContentBase64);
					
			if(response.getRequest() != null && response.getRequest().getAttribute().size() > 0)
			{			
				for(AttributeObject aObj : response.getRequest().getAttribute())
				{
					if(aObj.getAttributeId().equals("ns:c3isp:dpo-metadata"))
					{
						return aObj.getDataType(); 
					}
				}
			}
		} catch (ApiException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		return "";
	}
	
	private String __createRequestISI()
	{		
		String dtFormat = "yyyy-MM-dd'T'HH:mm:ss.0Z";
		String startTime = new SimpleDateFormat(dtFormat).format(Calendar.getInstance().getTime());
		Calendar cal = Calendar.getInstance(); 
		cal.add(Calendar.SECOND, 10);
		String endTime = new SimpleDateFormat(dtFormat).format(cal.getTime());
		
		MetadataModel metadataModel = new MetadataModel();
		metadataModel.setId(this.m_requestID);
		metadataModel.setDsaId("DSA-bd506204-7e07-48f0-a976-5ad54acd6d9d");		
		metadataModel.setEventType("Cyber Threat Monitoring");
		metadataModel.setOrganization("ISP@CNR");
		metadataModel.setStartTime(startTime);
		metadataModel.setEndTime(endTime);
		ObjectMapper objectMapper = new ObjectMapper();
		String value;
		try {
			value = objectMapper.writeValueAsString(metadataModel);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			value = "{"
					+ "\"id\":\"4000123\","
					+ "\"dsa_id\":\"DSA-bd506204-7e07-48f0-a976-5ad54acd6d9d\",\""
					+ "\"start_time\":\"2017-12-14T12:00:00.0Z\",\""
					+ "\"end_time\":\"2017-12-14T18:01:01.0Z\",\""
					+ "\"event_type\":\"Cyber Threat Monitoring\",\""
					+ "\"organization\":\"ISP@CNR\"	}"; 
		}				
		ResponseObject resObj = new ResponseObject();
				
		resObj.addAttributeItem(new AttributeObject()
				.attributeId("ns:c3isp:dpo-metadata")
				.dataType("string")
				.value(value));
		ISIResponseItem isiResponse = new ISIResponseItem();
		isiResponse.setRequest(resObj);
		try {
			return objectMapper.writeValueAsString(isiResponse);
		} catch (JsonProcessingException e) {
			return "{\"Request\":{\"Attribute\":[{\"AttributeId\":\"ns:c3isp:dpo-metadata\","
					+ "\"Value\":\"{"
						+ "\"id\":\"4000123\","
						+ "\"dsa_id\":\"DSA-c7aa89e7-ed98-42a8-91cf-74876c7ebc3e\","
						+ "\"start_time\":\"2017-12-14T12:00:00.0Z\","
						+ "\"end_time\":\"2017-12-14T18:01:01.0Z\","
						+ "\"event_type\":\"Cyber Threat Monitoring\","
						+ "\"organization\":\"ISP@CNR\"}\","
						+ "\"DataType\":\"string\"}]}}";
		} 
	}

	//ctiFile contains some information like this : dst=1:54.213.116.149
	public List<String> __getIpFromEncryptedPattern(
			@NotNull String fieldToEncrypt, 
			@NotNull String ctiFile)
	{		
		String[] actualContent = ctiFile.split("\\|");
		List<String> _lstStrIp = new ArrayList<String>();
		for (String s : actualContent) {			
			String[] parts = s.split(" ");
			for (int i=0; i<parts.length;i++) {
				if(parts[i].contains(fieldToEncrypt+"=") == false)
					continue;
								
				
				String motifEnryptedIp = parts[i].replaceAll(fieldToEncrypt+"=", ""); 
				if(motifEnryptedIp.contains(":")) {
					_lstStrIp.add(motifEnryptedIp.split(":")[1]);					
				}
			}
		}
		return _lstStrIp;
	}

	public boolean checkExistingDataBase(String staticDataBase) {		 
		File folderDataBase = new File(staticDataBase);
		if(folderDataBase.isDirectory() &&  folderDataBase.exists())
		{
			for (final File fileCiphetext : folderDataBase.listFiles())
			{
				if(fileCiphetext.getName().contains(".ct"))
					return true;
			}
		}
		return false; 
	}

	public void saveFHEKeysForAnalysis(String baseWorkingRequestFolder) throws IOException {
		
		boolean isCreated = m_fheKeyController.CreatingFHEKeyIfNeed();
    	if(isCreated)
    	{
    		/**
        	 * Existed keys, process to analyse now
        	 * */
    		KeyTransfer lstKeys = m_fheKeyController.getKeysForAnalysis();    
    		assert(lstKeys != null);
    		
    		FileOperations.PrintData(baseWorkingRequestFolder + File.separator + Constants.FheKeys.FHE_PK_FILE, 
    				ToolControlers.GetHEPublicKey(lstKeys.getLstKeys()));
    		FileOperations.PrintData(baseWorkingRequestFolder + File.separator + Constants.FheKeys.FHE_EVAL_FILE, 
    				ToolControlers.GetHEEvaluationKey(lstKeys.getLstKeys()));
    		FileOperations.PrintData(baseWorkingRequestFolder + File.separator + Constants.FheKeys.FHE_PARAM_FILE, 
    				ToolControlers.GetHEParameter(lstKeys.getLstKeys()));		    		
    	}
		
	}
}
