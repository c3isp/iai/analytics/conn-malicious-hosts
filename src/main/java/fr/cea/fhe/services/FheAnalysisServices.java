package fr.cea.fhe.services;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import fr.cea.kemanager.lib.Pair;
import fr.cea.fhe.helper.BashTools;

//import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
//import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;

import fr.cea.fhe.helper.Constants;
import fr.cea.fhe.helper.FileOperations;
import fr.cea.fhe.helper.ToolControlers;
import fr.cea.fhe.helper.ToolFile;
import fr.cea.fhe.helper.ZipUtility;
import fr.cea.fhe.models.FHEResponse;
import fr.cea.fhe.models.FHEResults;
import fr.cea.fhe.models.FheAnalysisModel;
import fr.cea.fhe.models.FheMetadata;
import fr.cea.fhe.models.KeyObject;
import fr.cea.fhe.models.KeyTransfer;
import fr.cea.fhe.repository.FheKeyRepository;

@Service
public class FheAnalysisServices {

	protected Logger log = LoggerFactory.getLogger(FheAnalysisServices.class.getName());	
	
	@Autowired
	FheKeyRepository 	m_fheKeyController;
	
	@Autowired
	ToolFile toolFile;
	
	@Autowired
	FheWorkers			m_FheWorker;
	
	private FheAnalysisModel m_AnalysisMethod;
	
	private String 			m_dsaId;	
	
	private FHEResponse m_vecDpoId;

	@Value("${iai.fhe.dedicatedDsaId}")
	private String DSA_ID;  //"DSA-44ee3dfc-4d3a-4219-979a-eb6ba7a8efc2";
							//"DSA-e3d5bc24-18dd-41f1-ba6c-5f7992fb71d0";
	
	private String m_metadata = "{            "
			+ "\"id\" : \"4000123\",       "
			+ "\"dsa_id\" :  \"{DSA_ID}\",       "
			+ "\"start_time\" : \"2017-12-14T12:00:00.0Z\",      "
			+ "\"end_time\" : \"2017-12-14T18:01:01.0Z\",       "
			+ "\"event_type\" : \"Firewall Event\",       "
			+ "\"organization\" : \"ISP@CNR\", "
			+ " \"fhe_data\": \"true\" }";
	
	
	@Value("${security.user.name}")
	private String restUser;
	@Value("${security.user.password}")
	private String restPassword;
	@Value("${isi.api.url}")
	private String isiApiURL;
	@Value("${isi.api.dpo}")	
	private String publishApi;
	
	@Value("${iai.fhe.datasizeFull}") 	private long fheDataSizeFull;	
	@Value("${iai.fhe.datasizeHigh}") 	private long fheDataSizeHigh;
	@Value("${iai.fhe.datasizeMedium}") private long fheDataSizeMedium;
	@Value("${iai.fhe.datasizeLow}") 	private long fheDataSizeLow;
		

	/**
	 * Used to call REST endpoints; configured by RestTemplateBuilder
	 */		
	private String defaultValue_VdlDposIDUrl = "/opt/isi/datalakebuffer/a74d0ae9-adce-4412-8e8f-c036bc1e7b8d";	
	private String defaultValue_DpoIDFolder = "contentFile1569832675329-f4e1a5e1-5d3d-4f83-9f8f-e4c83bec2c688818973977378419685";
	private String defaultValue_DpoIDObject = defaultValue_DpoIDFolder + ".fromzip";	
	
	public FheAnalysisServices() {			
	}
	
	/**
	 * This FHE method working with VDL which was already created from IAI component
	 * Remark : all the FHE data is in .zip format i.e 
	 * 1 - the 32 ciphertexts of encrypted IP in FHE format
	 * 2 - dsa.ini file used for DSA data
	 * 3 - all fhe_key.pk and fhe_key.eva, fhe_param.xml, boolean circuit .blif was already stored in zip dpo file
	 * @throws Exception 
	 * */
//	@HystrixCommand(    	
//    		groupKey="FHE_ANALYSIS_Service", commandKey = "FHE_CheckBlackList",
//    		fallbackMethod = "defaultCheckBlackListVDL",
//    		commandProperties = {
//				   @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "200000")
//			})
	
	public FHEResponse checkBelongToBlackListWithVDL(			
			FheAnalysisModel AnalysisMethod,   
    		String vdlDposIDUrl) throws Exception
	{
		/**
		 * Get all .dpo file in this folder and processing each of this file
		 * */
		File vdlFolder = new File(vdlDposIDUrl);
		m_vecDpoId = new FHEResponse().sessionId(UUID.randomUUID().toString());
		
		if(vdlFolder.exists() && vdlFolder.isDirectory() )
		{
			log.info("Foolder exist " + vdlDposIDUrl);
			File[] dpoFiles = vdlFolder.listFiles();
			for (File file : dpoFiles) {
				String dpoFileName = file.getName();				
				if(dpoFileName.contains(Constants.DPO_EXTENSION) == false) continue; // only consider .dpo object
				log.info("Checking file  " + dpoFileName);
				if(_isFHEData(AnalysisMethod, file)) // > 4MB it's FHE packaging data
				{
					log.info("running with fhe data at " + file.getAbsolutePath());
					FHEResults results = checkBelongToBlackListWithDPOInVDL(
												AnalysisMethod, vdlDposIDUrl, dpoFileName);					
					if(results != null )						
						m_vecDpoId.addFheResult(results);
					/*Testing for working with check the first .dpo object */
					//return dpoAnalysisRest;
				}
			}
		}
		else {
			return defaultCheckBlackListVDL("", "");
		}
		
		return m_vecDpoId;
	}
	
	/**
	 * @implNote: only filter fhe zip file
	 * */
	private boolean _isFHEData(FheAnalysisModel analysisMethod, File file) throws IOException {
		long sizeFile = Files.size(Paths.get(file.getAbsolutePath()));
		log.info("fhe size " + sizeFile);
		if(analysisMethod.equal(FheAnalysisModel.FULL) && sizeFile > fheDataSizeFull) 
		{
			log.info("fhe size for FULL");
			return true;
		}
		if(analysisMethod.equal(FheAnalysisModel.HIGH) && sizeFile > fheDataSizeHigh) { 
			log.info("fhe size for HIGH");
			return true;
		}
		
		if(analysisMethod.equal(FheAnalysisModel.MEDIUM) && sizeFile > fheDataSizeMedium) { 
			log.info("fhe size for MEDIUM"); 
			return true;
		}
		
		if(analysisMethod.equal(FheAnalysisModel.LOW) && sizeFile > fheDataSizeLow) { 
			log.info("fhe size for LOW"); 
			return true;
		}
		return false;
	}

	public FHEResults checkBelongToBlackListWithDPOInVDL(			
			FheAnalysisModel AnalysisMethod,   
    		String vdlDposIDUrl, 
    		String dpoIDObject) throws Exception
	{		
		try {
			String strVdlDposIDUrl = vdlDposIDUrl, strPathDpoIDObject = dpoIDObject;
			String fullPathFheZipCtFiles = "";
			
			if(AnalysisMethod == null && vdlDposIDUrl.length() == 0)
			{
				//throw new RuntimeException("Fallback is invoked !");
				return null;
			}			
			if(AnalysisMethod == null) {	
				AnalysisMethod = FheAnalysisModel.FULL;
			}
			if(vdlDposIDUrl.length() == 0) {		
				strVdlDposIDUrl = defaultValue_VdlDposIDUrl;
				strPathDpoIDObject = defaultValue_DpoIDObject;				 						
			}			
			this.m_AnalysisMethod = AnalysisMethod;
			
			try {
				Pair pairFolders = __extractDPOObject(strVdlDposIDUrl, strPathDpoIDObject);
				strVdlDposIDUrl = pairFolders.getName(); 
				fullPathFheZipCtFiles = pairFolders.getValue();
			} catch (IOException e) {
				e.printStackTrace();
				strVdlDposIDUrl = defaultValue_VdlDposIDUrl;
				strPathDpoIDObject = defaultValue_DpoIDObject;	
				try {
					log.info("NO DATA WAS FOUND !!! WORKING WITH DEFAULT VALUE ");
					BashTools.Exec(false, "/bin/rm", "-fr", strVdlDposIDUrl + File.separator + defaultValue_DpoIDFolder);
					BashTools.Exec(false, "/bin/rm", "-fr", strVdlDposIDUrl + File.separator + defaultValue_DpoIDFolder+Constants.FHE_ZIP_EXTENSION);					
					Pair pairFolders = __extractDPOObject(strVdlDposIDUrl, strPathDpoIDObject);
					strVdlDposIDUrl = pairFolders.getName(); 
					fullPathFheZipCtFiles = pairFolders.getValue();					
				} catch (IOException e1) {					
					e1.printStackTrace();
					return null;
				}
			}
			boolean result = __createFHEDataBaseIfNeed(
					strVdlDposIDUrl, 
					String.format(Constants.FheAlgo.BLACKLIST.DATABASE_DIR, m_dsaId, m_AnalysisMethod.toString()));
			if(result == false)
				throw new Exception("Impossible to create database");
			
			m_fheKeyController.init(UUID.randomUUID().toString(), this.m_dsaId, m_AnalysisMethod.toString());   
			//let's suppose these keys was created during creating database process 
			boolean isCreated = m_fheKeyController.CreatingFHEKeyIfNeed();
			if(isCreated)
			{
				/**
		    	 * Existed keys, process to get keys now
		    	 * */
				KeyTransfer lstKeys = m_fheKeyController.getKeysForAnalysis();    
				assert(lstKeys != null);
				
				FileOperations.PrintData(strVdlDposIDUrl + File.separator + Constants.FheKeys.FHE_PK_FILE, 
						ToolControlers.GetHEPublicKey(lstKeys.getLstKeys()));
				FileOperations.PrintData(strVdlDposIDUrl + File.separator + Constants.FheKeys.FHE_EVAL_FILE, 
						ToolControlers.GetHEEvaluationKey(lstKeys.getLstKeys()));
				FileOperations.PrintData(strVdlDposIDUrl + File.separator + Constants.FheKeys.FHE_PARAM_FILE, 
						ToolControlers.GetHEParameter(lstKeys.getLstKeys()));				
			}
			//String fileLock = vdlDposIDUrl + File.separator + Constants.LOCK_FILE;
			
			String resultFolder = m_FheWorker.runAnalysis(strVdlDposIDUrl, this.m_dsaId, m_AnalysisMethod);			
			log.info("Result Folder : " + resultFolder);
			File fRestfolder = new File(resultFolder);
			int countFileCt = 0; 
			for (File iFile : fRestfolder.listFiles()) {
				String logsChecker = "checking file " + iFile.getName() + " in folder " + fRestfolder.getAbsolutePath();
				
				if(iFile.getName().contains(Constants.FHE_CT_EXTENSION) ) 
				{
					countFileCt ++;
					logsChecker += " : is FHE file";
				}
				log.info(logsChecker);
			}
			if(countFileCt == 0)
			{	
				log.info("No Ciphertext was nounf at result folder " + fRestfolder.getAbsolutePath());
				return new FHEResults().dpoIp("Internal-Exec-No-data-CipherText was found").dpoResult(" pls check folder " + resultFolder);
			}
			String dpoId = "", dpoIdIp = "" ;
			try {
				File fileZipFheResult = __createZipFile(resultFolder);		
				File fileZipFheIpFile = __createIPZipCtFiles(fullPathFheZipCtFiles);
				log.info("Invoke ISI create DPO for this result !");
			/**
			 * Create general DPO ID without FHE encryption
			 * */	
			String	_tmpMetadata = m_metadata.replaceAll("\\{" + "DSA_ID" + "\\}", this.DSA_ID);
			dpoId = ToolControlers.createDpoObject(isiApiURL+publishApi, fileZipFheResult, _tmpMetadata,  this.DSA_ID);
			dpoIdIp = ToolControlers.createDpoObject(isiApiURL+publishApi, fileZipFheIpFile, _tmpMetadata,  this.DSA_ID);
			} catch (JsonParseException e) { 
				e.printStackTrace();
			} catch (JsonMappingException e) {				
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			this.m_dsaId = "";
			log.info("DPOS-ID = " + dpoId + " for DPOS-ID-IP = " + dpoIdIp);	
			/**
			 * This order is important to decrypt
			 * */
			return new FHEResults().dpoIp(dpoIdIp).dpoResult(dpoId); 
		} catch (ExceptionInInitializerError e) {
			e.printStackTrace();			
		} 		
		return null; 		
	}

	
	private File __createIPZipCtFiles(String fullPathFheZipCtFiles) {
		try {
			String filename = toolFile.getFileName(fullPathFheZipCtFiles);
			String extension = Constants.FHE_ZIP_EXTENSION;
			String folderName = filename.replace(extension, "") + "_" + Constants.RESULT_ANALYSIS_FOLDER;					
			String fullPathToFolder = toolFile.pathFormater(fullPathFheZipCtFiles) + File.separator + folderName;
						
			BashTools.Exec(false, "/usr/bin/unzip", "-o", "-q", fullPathFheZipCtFiles, "-d", fullPathToFolder);
			if(FileOperations.IsExisted(fullPathToFolder) == false || new File(fullPathToFolder).isDirectory() == false)
			{
				throw new IOException("Impossible to unzip " + fullPathFheZipCtFiles +" to folder : " + fullPathToFolder);
			}
			int bitCount = Constants.FheAlgo.BLACKLIST_FULL_BITCOUNT;
			if (this.m_AnalysisMethod.equal(FheAnalysisModel.HIGH))
			{
				bitCount = 32;	
			}
			else if (this.m_AnalysisMethod.equal(FheAnalysisModel.MEDIUM))
			{
				bitCount = 16;	
			}	
			else if (this.m_AnalysisMethod.equal(FheAnalysisModel.LOW))
			{
				bitCount = 8;	
			}	
			return __createZipFile(fullPathToFolder, bitCount);
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		return null;
	}

	/**
	 * Zip all FHE files + 1 fhemetadata file for DSA-ID to decrypt result
	 */
	private File __createZipFile(String resultFolder) throws IOException {
		return __createZipFile (resultFolder, 1); 
	}
	private File __createZipFile(String resultFolder, int bitCount) throws IOException {
		String fheMetadataFile = resultFolder + File.separator + Constants.FHE_METADATA_FILE; 
		String partialPathToFile = resultFolder + File.separator + Constants.PARTIAL_DATA_FILE;
		String partialData = "" ; //put partial data into fhemetadataFile
		if(FileOperations.IsExisted(partialPathToFile) == true)
		{
			partialData = new String(FileOperations.GetData(partialPathToFile), StandardCharsets.UTF_8).trim(); 
		}
		/**
		 * Save fhe metadata file with dsa id and analysis method 
		 * */
		FheMetadata fhemetada = new FheMetadata()
									.dsaId(m_dsaId)
									.fheAnalysisMethod(m_AnalysisMethod.toString())
									.bitCount(bitCount)
									.partialData(partialData); 
		
		ObjectMapper mapper = new ObjectMapper();
		String jsonObj = mapper.writeValueAsString(fhemetada);
				
		FileOperations.PrintData(fheMetadataFile, jsonObj.getBytes());
		/**
		 * Zip all files in the result folder
		 * */		
		File folderDirectory = new File(resultFolder); 			
		File[] ciphertextFiles = folderDirectory.listFiles();
		List<File> srcFiles = new ArrayList<File>();
		for (File file : ciphertextFiles) {
			srcFiles.add(file);
		}
		try {
			File fileZip = ZipUtility.zipContent(srcFiles, UUID.randomUUID().toString());	
			log.info("Zip Folder " + resultFolder + " stored at " + fileZip.getAbsolutePath());
			return fileZip; 
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}

	private Pair __extractDPOObject(String strVdlDposIDUrl, String strDpoIDObjectFileName) throws IOException{
		/**
		 * When storing fhe data in VDL the FHE data package has the extension name : <dpo-id>.dpo  
		 * */
		String pathFHEDpoFile = strVdlDposIDUrl + File.separator + strDpoIDObjectFileName; 
		
		/**
		 * 1> Rename to zip file 
		 * 2> unzip this file
		 * 3> Move all files with suffix .ct into input folder
		 * */
		String folderName = "";
		String fullPathToFolder = "";
		String fullPathZipFile = ""; //used for creating DPO-IP for later
		log.info("Checking folder existed ? " + pathFHEDpoFile);
		File dpoFheFile = new File(pathFHEDpoFile);
		if(dpoFheFile.exists() == false)
		{
			throw new IOException(dpoFheFile + " is not EXISTED !");
		}
		
		if(dpoFheFile.exists() && dpoFheFile.isFile() == false)
		{
			throw new IOException(dpoFheFile + " is not a FHE file");
		}
		if(dpoFheFile.exists() && dpoFheFile.isFile())
		{			
			String filename = toolFile.getFileName(pathFHEDpoFile);
			String extension = FilenameUtils.getExtension(filename);
			folderName = filename.replace("."+extension, "");
			fullPathZipFile = strVdlDposIDUrl + File.separator + folderName + Constants.FHE_ZIP_EXTENSION;			
			fullPathToFolder = toolFile.pathFormater(pathFHEDpoFile) + File.separator + folderName;
			if(new File(fullPathZipFile).exists() && new File(fullPathToFolder).exists())				
			{
				BashTools.Exec(false, "/bin/rm", fullPathZipFile);
				BashTools.Exec(false, "/bin/rm", "-fr", fullPathToFolder);
			}
			
			BashTools.Exec(false, "/bin/cp", pathFHEDpoFile, fullPathZipFile);
			BashTools.Exec(false, "/usr/bin/unzip", "-o", "-q", fullPathZipFile, "-d", fullPathToFolder);
			if(FileOperations.IsExisted(fullPathToFolder) == false || new File(fullPathToFolder).isDirectory() == false)
			{
				throw new IOException("Impossible to unzip " + pathFHEDpoFile +" to folder : " + fullPathToFolder);
			}
			String inputfolder = fullPathToFolder + File.separator + Constants.INPUT_ANALYSIS_FOLDER;
			
			BashTools.Exec(false, "/bin/mkdir", "-p", inputfolder);
			
			/**
			 * Copy encrypted IP files to input folder 
			 * */
			File inputFolderDirectory = new File(inputfolder);
			if(Files.isDirectory(Paths.get(inputfolder)) == false)
				throw new IOException("Impossible to create directory : " + inputfolder);
			
			File folderDirectory = new File(fullPathToFolder); 			
			File[] dpoFiles = folderDirectory.listFiles();
			for (File file : dpoFiles) {
				if(toolFile.getFileName(file.getAbsolutePath()).contains(Constants.FHE_CT_EXTENSION))
				{
					try {
						FileUtils.copyFileToDirectory(file, inputFolderDirectory);
					} catch (IOException e) {
						e.printStackTrace();
						log.info("Error for copy file " + file.getAbsolutePath() + " to " + inputfolder);
					}
				}
			}			
			File[] ctFiles = inputFolderDirectory.listFiles();
			if(ctFiles.length == 0) {
				log.error("Error 0 file in " + inputFolderDirectory.getAbsolutePath());
				throw new IOException("Ciphertext File is not existed in folder : " + 
												inputFolderDirectory.getAbsolutePath());
			}
		} 
		else throw new IOException("File not existed: " + pathFHEDpoFile);
		
		/**
		 * 4> Checking dsa.ini file to get DSA ID
		 * */
		String dsaDataFile = fullPathToFolder + File.separator + Constants.DSA_FILE;
		this.m_dsaId = new String(FileOperations.GetData(dsaDataFile), StandardCharsets.UTF_8);
		log.info("DSA ID = " + m_dsaId);
		return new Pair(fullPathToFolder, fullPathZipFile); 		
	}

	private boolean __createFHEDataBaseIfNeed(String strVdlDposIDUrl, String rootDataBase) {		
		
		//String dataBaseDir = String.format(Constants.FheAlgo.BLACKLIST.DATABASE_DIR, dsaID, AnalysisMethod); 
		if(ToolControlers.checkExistingDataBase(rootDataBase) == false)
		{
			log.info("NO DATA BASE WAS EXISTED ! START CREATING DATABASE for " + this.m_dsaId);
			ResponseEntity<String> responseCreateDB;
			try {				
				responseCreateDB = ToolControlers.CreateDatabase(null, m_fheKeyController, 
						this.m_dsaId, m_AnalysisMethod.toString(), rootDataBase, false);
				if(responseCreateDB.getStatusCode() != HttpStatus.CREATED)
		    	{
		    		throw new Exception ( "{\"Response\": "
		    				+ "\"NO DATA BASE WAS CREATED ! PLEASE MANUALLY CREATE DATABASE FIRST !\"}");
		    	}				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				log.error("ERROR IN DATA BASE CREATION ! " + rootDataBase + " need to move ct file into : " + strVdlDposIDUrl);
				return false;
			}
		}
		log.info("DATA BASE WAS EXISTED ! " + rootDataBase + " need to copy ct file & partial database into : " + 
				strVdlDposIDUrl + File.separator + Constants.INPUT_ANALYSIS_FOLDER);
		
		String inputfolder = strVdlDposIDUrl + File.separator + Constants.INPUT_ANALYSIS_FOLDER;
//		BashTools.Exec(false, "/bin/cp", rootDataBase+File.separator + "*.ct",  inputfolder); // no exec or copy actions
		
		File inputFolderDirectory = new File(inputfolder);
		File[] ctFiles = new File(rootDataBase).listFiles();		
		for (File file : ctFiles) {
			if(toolFile.getFileName(file.getAbsolutePath()).contains(Constants.FHE_CT_EXTENSION)
				|| (toolFile.getFileName(file.getAbsolutePath()).contains(m_AnalysisMethod.toString() /*use for checking clear text*/)))
			{
				try {
					FileUtils.copyFileToDirectory(file, inputFolderDirectory);
				} catch (IOException e) {
					e.printStackTrace();
					log.info("Error for copy file " + file.getAbsolutePath() + " to " + inputfolder);
				}
			}			
		}		
		log.info("End of copy file " + inputfolder);
		File[] ctInputFiles = new File(inputfolder).listFiles();	
		if(ctInputFiles.length == 0)
			log.error("Error for copy file from "+ rootDataBase +" into folder " + inputfolder + " : No ciphertexte was found !");
//		for (File file : ctFiles) {
//			if(toolFile.getFileName(file.getAbsolutePath()).contains(Constants.FHE_CT_EXTENSION)
		return true;
		//String inputFolder = strVdlDposIDUrl
	}

	public FHEResponse defaultCheckBlackListVDL(
			String AnalysisMethod,   
    		String vdlDposIDUrl)
	{
		m_vecDpoId = new FHEResponse().sessionId(UUID.randomUUID().toString());
		m_vecDpoId.addFheResult(new FHEResults().dpoIp("dpos-ip-0").dpoResult("dpoResult-id-0"));
		m_vecDpoId.addFheResult(new FHEResults().dpoIp("dpos-ip-1").dpoResult("dpoResult-id-1"));
		m_vecDpoId.addFheResult(new FHEResults().dpoIp("dpos-ip-2").dpoResult("dpoResult-id-2"));
		
    	return m_vecDpoId;		
	}
}
