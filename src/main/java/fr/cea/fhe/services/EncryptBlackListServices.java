package fr.cea.fhe.services;

import java.io.File;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.cea.fhe.helper.BashTools;
import fr.cea.fhe.helper.Constants;
import fr.cea.fhe.helper.ToolsFolder;

public class EncryptBlackListServices implements Runnable  {

	private static final Logger log = LoggerFactory.getLogger(EncryptBlackListServices.class);
	
	private Thread 				m_thread;
	private String 				m_rootDir;
	private String 				m_baseDir;
	private String 				m_threadName = "[FHE-BL-DataBase]: ";
	private String 				m_dsaId; 
	private String 				m_fheModel;
	
	public EncryptBlackListServices() {}

	public EncryptBlackListServices(String baseDir, String dsaId, String fheModel) {		
		this.m_baseDir 				= baseDir;
		this.m_dsaId 				= dsaId; 
		this.m_fheModel 			= fheModel;
	}	

	@Override
	public void run() {
		if (m_thread == null)	m_thread = new Thread (this);		
		String databaseStorage = String.format(Constants.FheAlgo.BLACKLIST.DATABASE_DIR, this.m_dsaId, this.m_fheModel);
		
		try {
			try {
				ToolsFolder.CreateAllFolderNames(databaseStorage);
			} catch (SecurityException | IOException e) {
				// TODO Auto-generated catch block
				log.error(m_threadName + "Error during create folders for " + databaseStorage);
				return;
			}
							
			log.info(m_threadName + "Preparing to encrypt blacklist database now for : " + m_dsaId);
			log.info(m_threadName + String.format("/bin/bash %s %s %s", 
					m_baseDir + File.separator + Constants.FheScripts.FHE_PREPARE_DATABASE_EXEC, databaseStorage, m_fheModel)); 
			
			boolean isDone = BashTools.Exec(true,
					m_baseDir + File.separator + Constants.FheScripts.FHE_PREPARE_DATABASE_EXEC,
					databaseStorage, m_fheModel);      	        
			if(isDone == false)
			{
				log.info(m_threadName + "Cannot exec script for preparing blacklist database for : " + m_dsaId);
			} 
			else 
				log.info(m_threadName + "Finish preparing blacklist database for : " + m_dsaId);
			//m_thread.interrupt();
		} catch (SecurityException e) {			
		}
		log.info ("End exec creating blacklist DataBase !");
	}
}
