package fr.cea.fhe.repository;

import java.io.File;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.support.BasicAuthorizationInterceptor;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import fr.cea.fhe.models.KeyTransfer;

@Component
public class FheKeyRepository {

	Logger log = LoggerFactory.getLogger(FheKeyRepository.class); 
	private final String logStatus = "[FHE-ENCRYPT-MANAGER] ";	

	private String baseUrl; 
	private String requestId; 
	private String dsAId;
	private String fhEModel;
	private RestTemplate m_restTemplate;
	
	private String checkKeyUrl;
	private String getKeyUrl;
	private String createKeyUrl;
	
	@Value("${rest.endpoint.url.callKEcheckKeyUrl}")
	private String checkKeyUrlOrigine;// 	= "/fhe-keys/v1/fhe/key/isExisted/{RequestId}/dsa/{DSAId}/scheme/{FHEModel}";
	
	@Value("${rest.endpoint.url.callKEgetKeyUrl}")
	private String getKeyUrlOrigine;// 	= "/fhe-keys/v1/fhe/key/FHE_PUBKEY/get/{RequestId}/dsa/{DSAId}/scheme/{FHEModel}";
	
	@Value("${rest.endpoint.url.callKEcreateKeyUrl}")
	private String createKeyUrlOrigine; // = "/fhe-keys/v1/fhe/key/create/{RequestId}/dsa/{DSAId}/scheme/{FHEModel}";
		
	@Autowired
	public FheKeyRepository(
		    @Value("${rest.endpoint.url.callFheKeyCoreBase}") 	String baseUrl, 
			@Value("${security.user.name}") 					String restUser,
			@Value("${security.user.password}") 				String restPassword) {
		super();
		this.baseUrl = baseUrl;
		m_restTemplate = new RestTemplate();
		m_restTemplate.getInterceptors().add(
				  new BasicAuthorizationInterceptor(restUser, restPassword));
	}

	
	public void init (String requestId, String dsAId, String fhEModel) {				
		this.requestId = requestId;
		this.dsAId = dsAId;
		this.fhEModel = fhEModel;
		
		checkKeyUrl = checkKeyUrlOrigine.replaceAll("\\{" + "RequestId" + "\\}", requestId)
								.replaceAll("\\{" + "DSAId" + "\\}", dsAId)
								.replaceAll("\\{" + "FHEModel" + "\\}", fhEModel);
			  
		getKeyUrl = getKeyUrlOrigine
				.replaceAll("\\{" + "RequestId" + "\\}", requestId)
				.replaceAll("\\{" + "DSAId" + "\\}", dsAId)
				.replaceAll("\\{" + "FHEModel" + "\\}", fhEModel);

		createKeyUrl = createKeyUrlOrigine
				.replaceAll("\\{" + "RequestId" + "\\}", requestId)
				.replaceAll("\\{" + "DSAId" + "\\}", dsAId)
				.replaceAll("\\{" + "FHEModel" + "\\}", fhEModel);
			
	}
	
	public boolean CreatingFHEKeyIfNeed()
	{
		String fullCheckKeyURL 	= baseUrl + checkKeyUrl;		
		String fullCreateKeyURL = baseUrl + createKeyUrl;	
		
		try {
			log.info(logStatus + "Starting request for " + fullCheckKeyURL);
			ResponseEntity<String> resCheck = m_restTemplate.getForEntity(fullCheckKeyURL, String.class);
			log.info(logStatus + " Body Response = " + resCheck.getBody() + " Checking Keys getStatusCode = " + resCheck.getStatusCode());
			if(resCheck.getStatusCode() == HttpStatus.NO_CONTENT)
			{
				log.info(logStatus + "Invoking CreateFHEKey now ");
				ResponseEntity<String> resCreateKeys = m_restTemplate.getForEntity(fullCreateKeyURL, String.class);
				if(resCreateKeys.getStatusCode() != HttpStatus.CREATED)
    			{
					throw new IOException(logStatus + "Impossible to connect and Create FHE keys ");
    			}
				else if(resCreateKeys.getStatusCode() == HttpStatus.CREATED)
					log.info(logStatus + "CreateFHEKey successfully !");
			}
			else if(resCheck.getStatusCode() == HttpStatus.CREATED)
			{
				log.info(logStatus + "#Checking Keys : Is Existed !");
			}
			return true;
		} catch (HttpClientErrorException e) {
			log.info(logStatus + "HttpClientErrorException encrypt to KE-KEY-API \n " + e.toString());
		} catch (RestClientException e) {
			log.info(logStatus + "Error executing request encrypt to KE-CORE-API \n " + e.toString());			
		} catch (IOException e) {
			e.printStackTrace();
			log.info(logStatus + "Error executing request IOException to KE-CORE-API \n " + e.toString());
		}                	   
		return false;
	}
	
	public KeyTransfer getKeysForAnalysis()
	{
		String fullGetKeyURL 	= baseUrl + getKeyUrl;   
		log.info(logStatus + "getKeysForAnalysis : Get Request " + fullGetKeyURL);
		ResponseEntity<KeyTransfer> reslstKeys = m_restTemplate.getForEntity(fullGetKeyURL, KeyTransfer.class);
		
		if(reslstKeys.getStatusCode() == HttpStatus.OK)
		{
			KeyTransfer lstKeys = reslstKeys.getBody();
			log.info(logStatus + "Get keys success !");
			return lstKeys;
		}
		return null;
	}
	
}
