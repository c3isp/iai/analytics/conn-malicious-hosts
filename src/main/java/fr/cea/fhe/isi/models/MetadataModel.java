package fr.cea.fhe.isi.models;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MetadataModel {

	public String getId() {
		return Id;
	}

	public void setId(String id) {
		Id = id;
	}

	public String getDsaId() {
		return dsaId;
	}

	public void setDsaId(String dsaId) {
		this.dsaId = dsaId;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getEventType() {
		return eventType;
	}

	public void setEventType(String eventType) {
		this.eventType = eventType;
	}

	public String getOrganization() {
		return organization;
	}

	public void setOrganization(String organization) {
		this.organization = organization;
	}

	public String getConstrainedString() {
		return constrainedString;
	}

	public void setConstrainedString(String constrainedString) {
		this.constrainedString = constrainedString;
	}

	public String getConstrainedFloat() {
		return constrainedFloat;
	}

	public void setConstrainedFloat(String constrainedFloat) {
		this.constrainedFloat = constrainedFloat;
	}

	public MetadataModel() {
		super();
	}

	@JsonProperty("id")
	private String Id; 
	
	@JsonProperty("dsa_id")
	private String dsaId;
	
	@JsonProperty("start_time")
	private String startTime; 
	
	@JsonProperty("end_time")
	private String endTime;
	
	@JsonProperty("event_type")
	private String eventType;
	
	@JsonProperty("organization")
	private String organization;
	
	@JsonProperty("constrained_string")
	private String constrainedString;
	
	@JsonProperty("constrained_float")
	private String constrainedFloat;
	
}
